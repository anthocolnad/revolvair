import { SensorResolver } from './Sensor';
import {Measure} from '../../models/Measure';

const maxValue = 4.14;
const minValue = 3.14;

const fakeStations =
[
    {
        id: 1,
        name: 'china2',
        userId: 5,
    },
    {
        id: 2,
        name: 'quebec',
        userId: 5,
    },
    {
        id: 2,
        name: 'china',
        userId: 5,
    },
];

const fakeSensor =
[
    {
        id: 1,
        long: -71.34,
        lat: 41.12,
    },
    {
        id: 2,
        long: -71.34,
        lat: 41.12,
    },
];

const fakeMeasures =
[
    {
        sensorId: 1,
        value: minValue,
        timestamp: '2018-10-15T23:27:30Z',
    },
    {
        sensorId: 1,
        value: maxValue,
        timestamp: '2018-10-15T23:25:30Z',
    },
];


describe('Sensor resolver when list with multiple element', () => {
    let sensorResolver: SensorResolver;
    beforeEach(() => {
        const clientDbMock = {
            getAllStations: jest.fn().mockReturnValue(fakeStations),
            getAllStationById: jest.fn().mockReturnValue(fakeStations),
            getSensorsIdsByStationId: jest.fn().mockReturnValue(fakeSensor),
            getSensorIdByStationIdAndSensorId: jest.fn().mockReturnValue(fakeSensor),
            getMeasureBySensorId: jest.fn().mockReturnValue(fakeMeasures),
            getMeasureByTimeOption: jest.fn().mockReturnValue(fakeMeasures),
            addMeasure: jest.fn().mockReturnValue(fakeMeasures),
            getNotificationByStationId: jest.fn().mockReturnValue(null),
            addNotification: jest.fn().mockReturnValue(null),
        };
        sensorResolver = new SensorResolver(clientDbMock);
    });
    describe('When using sensors and ask for measures', () => {
        it('Should return all measures', (done) => {
            const sensor = {id: 5, stationId: 1, long: 1, lat: 1};
            const expectedMeasure = fakeMeasures[0];

            sensorResolver.getResolver().Sensor.latestMeasures(sensor).then((measures: Measure[]) => {
                expect(measures).toContain(expectedMeasure);
                done();
            });
        });
    });
});

describe('Sensor resolver when db client return empty list', () => {
    let sensorResolver: SensorResolver;
    beforeEach(() => {
        const clientDbMock = {
            getAllStations: jest.fn().mockReturnValue([]),
            getAllStationById: jest.fn().mockReturnValue([]),
            getSensorsIdsByStationId: jest.fn().mockReturnValue([]),
            getSensorIdByStationIdAndSensorId: jest.fn().mockReturnValue([]),
            getMeasureBySensorId: jest.fn().mockReturnValue(undefined),
            getMeasureByTimeOption: jest.fn().mockReturnValue(undefined),
            addMeasure: jest.fn().mockReturnValue(fakeMeasures),
            getNotificationByStationId: jest.fn().mockReturnValue(null),
            addNotification: jest.fn().mockReturnValue(null),
        };
        sensorResolver = new SensorResolver(clientDbMock);
    });
    describe('When using sensor and ask for measures', () => {
        it('Should return empty list', (done) => {
            const sensor = {id: 1, stationId: 1, long: 1, lat: 1};
            sensorResolver.getResolver().Sensor.latestMeasures(sensor).then((measures: Measure[]) => {
                expect(measures.length).toBe(0);
                done();
            });
        });
    });
    describe('When using sensor and ask for measure by timerange', () => {
        it('Should return empty list', (done) => {
            const timerange = {timeRange: 'now-4d'};
            const root = {stationId: 1, id: 1};
            sensorResolver.getResolver().Sensor.measures(root, timerange).then((measures: Measure[]) => {
                expect(measures.length).toBe(0);
                done();
            });
        });
    });
});
