import {Mutations} from './Mutations';
import {Measure} from '../../models/Measure';
import {DBClient} from '../DBClient';

const jwt = require('jsonwebtoken');

process.env.JWTSECRET = 'test';
const sensorId = 12;
const value = 99;
const secret = process.env.JWTSECRET;

describe('Sensor mutation when db client return fake measure', () => {
    let sensorResolver: Mutations;
    const value1 = 1;
    const measure = { sensorId: sensorId, value: value, timestamp: '234' };
    const measure1 = [{ sensorId: sensorId, value: value1, timestamp: '234' }];
    const fakeJWTToken = jwt.sign({
            sensorId: sensorId,
        },
        secret,
        {
            expiresIn: '5y',
        });
    let clientDbMock: DBClient;
    beforeEach(() => {
        clientDbMock = {
            getAllStations: jest.fn().mockReturnValue([]),
            getAllStationById: jest.fn().mockReturnValue([]),
            getSensorsIdsByStationId: jest.fn().mockReturnValue([]),
            getSensorIdByStationIdAndSensorId: jest.fn().mockReturnValue([]),
            getMeasureBySensorId: () => new Promise((resolve) => resolve(measure1)),
            getMeasureByTimeOption: jest.fn().mockReturnValue([]),
            addMeasure: jest.fn().mockReturnValue(measure),
            getNotificationByStationId: jest.fn().mockReturnValue([]),
            addNotification: jest.fn().mockReturnValue(null),
        };
        sensorResolver = new Mutations(clientDbMock);
    });
    describe('AddMeasure', () => {
        it('When good authorization should return value', (done) => {
            sensorResolver.getResolver().Mutation.addMeasure('' , {sensorId: sensorId, stationId: sensorId, value: value}, {headers: { authorization: fakeJWTToken} }).then((actualMeasure) => {
                expect(actualMeasure).toBe(measure);
                done();
            });
        });
        it('When last and new measures have different range notification is added', (done) => {
            sensorResolver.getResolver().Mutation.addMeasure('' , {sensorId: sensorId, stationId: sensorId, value: value}, {headers: { authorization: fakeJWTToken} }).then((actualMeasure) => {
                expect(clientDbMock.addNotification).toBeCalled();
                done();
            });
        });
        it('When last and new measures have same range notification is not added', (done) => {
            clientDbMock.getMeasureBySensorId = () => new Promise((resolve) => resolve([measure])),
            sensorResolver.getResolver().Mutation.addMeasure('' , {sensorId: sensorId, stationId: sensorId, value: value}, {headers: { authorization: fakeJWTToken} }).then((actualMeasure) => {
                expect(clientDbMock.addNotification).toHaveBeenCalledTimes(0);
                done();
            });
        });
        it('When station doesn\'t have measures notification is not added', (done) => {
            clientDbMock.getMeasureBySensorId = () => new Promise((resolve) => resolve([])),
            sensorResolver.getResolver().Mutation.addMeasure('' , {sensorId: sensorId, stationId: sensorId, value: value}, {headers: { authorization: fakeJWTToken} }).then((actualMeasure) => {
                expect(clientDbMock.addNotification).toHaveBeenCalledTimes(0);
                done();
            });
        });
    });
    describe('GenerateSensorToken', () => {
        it('When empty authorization should return undefined', (done) => {
            sensorResolver.getResolver().Mutation.generateSensorToken(null, {sensorId: sensorId} ).then((measure: Measure) => {
                expect(measure).not.toBe(undefined);
                done();
            });
        });
    });
});
