import { Station } from '../../models/Station';
import { StationResolver } from './Station';
import {Sensor} from '../../models/Sensor';
import {Notification} from '../../models/Notification';

const fakeStations =
[
    {
        id: 1,
        name: 'china2',
        userId: 5,
        long: 1,
        lat: 1,
    },
    {
        id: 2,
        name: 'quebec',
        userId: 5,
        long: 1,
        lat: 1,
    },
    {
        id: 3,
        name: 'china',
        userId: 5,
        long: 1,
        lat: 1,
    },
];

const fakeSensor = [
    {
        id: 1,
    },
    {
        id: 2,
    },
    {
        id: 3,
    },
];

const newState = 'good';
const oldState = 'bad';
const newValue = 1;
const oldValue = 30;
const stationId = 3;
const timestamp = '1234';
const fakeNotification = [
    {
        newState: newState,
        oldState: oldState,
        newValue: newValue,
        oldValue: oldValue,
        stationId: stationId,
        timestamp: timestamp,
    },
];

const station = {id: 1, name: 'china2', userId: 5, long: 1, lat: 2};

describe('Sensor resolver when list with multiple element', () => {
    let stationResolver: StationResolver;
    beforeEach(() => {
        const clientDbMock = {
            getAllStations: jest.fn().mockReturnValue(fakeStations),
            getAllStationById: jest.fn().mockReturnValue(fakeStations),
            getSensorsIdsByStationId: jest.fn().mockReturnValue(fakeSensor),
            getSensorIdByStationIdAndSensorId: jest.fn().mockReturnValue(fakeSensor),
            getMeasureBySensorId: jest.fn().mockReturnValue(null),
            getMeasureByTimeOption: jest.fn().mockReturnValue(null),
            addMeasure: jest.fn().mockReturnValue(null),
            getNotificationByStationId: jest.fn().mockReturnValue(fakeNotification),
            addNotification: jest.fn().mockReturnValue(null),
        };
        stationResolver = new StationResolver(clientDbMock);
    });
    describe('When Query stations', () => {
        it('Should return all stations', (done) => {
            stationResolver.getResolver().Query.stations().then((stations: Station[]) => {
                expect(stations.length).toBe(fakeStations.length);
                done();
            });

        });
    });
    describe('When Query station with id', () => {
        it('Should return station with good id', (done) => {
            stationResolver.getResolver().Query.station(null, station).then((stations: Station[]) => {
                const currrentStation = stations[0];
                expect(currrentStation.id).toBe(station.id);
                expect(currrentStation.name).toBe(station.name);
                expect(currrentStation.userId).toBe(station.userId);
                done();
            });
        });
    });
    describe('When get sensors', () => {
        it('Should return a list of sensors', (done) => {
            stationResolver.getResolver().Station.sensors(station).then((stations: Sensor[]) => {
                const currrentStation = stations[0];
                expect(currrentStation.id).toBe(station.id);
                done();
            });
        });
    });
    describe('When get notifications', () => {
        it('Should return a list of notifications', (done) => {
            stationResolver.getResolver().Station.notifications(station).then((notifications: Notification[]) => {
                const currentNotification = notifications[0];
                expect(currentNotification.newState).toBe(newState);
                expect(currentNotification.oldState).toBe(oldState);
                expect(currentNotification.newValue).toBe(newValue);
                expect(currentNotification.oldValue).toBe(oldValue);
                expect(currentNotification.timestamp).toBe(timestamp);
                done();
            });
        });
    });
});


describe('Sensor resolver when return undefined', () => {
    let stationResolver: StationResolver;
    beforeEach(() => {
        const clientDbMock = {
            getAllStations: jest.fn().mockReturnValue(undefined),
            getAllStationById: jest.fn().mockReturnValue(undefined),
            getSensorsIdsByStationId: jest.fn().mockReturnValue(undefined),
            getSensorIdByStationIdAndSensorId: jest.fn().mockReturnValue(undefined),
            getMeasureBySensorId: jest.fn().mockReturnValue(null),
            getMeasureByTimeOption: jest.fn().mockReturnValue(null),
            addMeasure: jest.fn().mockReturnValue(null),
            getNotificationByStationId: jest.fn().mockReturnValue(undefined),
            addNotification: jest.fn().mockReturnValue(null),
        };
        stationResolver = new StationResolver(clientDbMock);
    });
    describe('When Query stations', () => {
        it('Should return empty list', (done) => {
            stationResolver.getResolver().Query.stations().then((stations: Station[]) => {
                expect(stations.length).toBe(0);
                done();
            });

        });
    });
    describe('When Query station with id', () => {
        it('Should return empty list', (done) => {
            stationResolver.getResolver().Query.station(null, station).then((stations: Station[]) => {
                expect(stations.length).toBe(0);
                done();
            });
        });
    });
    describe('When get sensors', () => {
        it('Should return empty list', (done) => {
            stationResolver.getResolver().Station.sensors(station).then((stations: Sensor[]) => {
                expect(stations.length).toBe(0);
                done();
            });
        });
    });
    describe('When get sensor id', () => {
        it('Should return empty list', (done) => {
            const sensor = {id: 1, stationId: 1};
            stationResolver.getResolver().Station.sensor(station, sensor).then((stations: Sensor[]) => {
                expect(stations.length).toBe(0);
                done();
            });
        });
    });
    describe('When get notifications', () => {
        it('Should return empty list', (done) => {
            stationResolver.getResolver().Station.notifications(station).then((notifications: Notification[]) => {
                expect(notifications.length).toBe(0);
                done();
            });
        });
    });
});
