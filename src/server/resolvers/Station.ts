import { Station } from '../../models/Station';
import { Sensor } from '../../models/Sensor';
import { ObjectResolver, Resolver } from './Resolver';

export class StationResolver extends ObjectResolver implements Resolver {

    private stationResolver = {
        Query: {
            stations: async () => {
                return await this.getDbClient().getAllStations() || [];
            },
            station: async (root: void, args: Station) => {
                return await this.getDbClient().getAllStationById(args.id) || [];
            },
        },
        Station: {
            sensors: async (root: Station) => {
                return await this.getDbClient().getSensorsIdsByStationId(root.id) || [];
            },
            sensor: async (root: Station, args: Sensor) => {
                return await this.getDbClient().getSensorIdByStationIdAndSensorId(root.id, args.id) || [];
            },
            notifications: async (root: Station) => {
                return await this.getDbClient().getNotificationByStationId(root.id) || [];
            },
        },
    };

    public getResolver() {
        return this.stationResolver;
    }
}
