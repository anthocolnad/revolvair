import { ObjectResolver, Resolver } from './Resolver';
import * as dotenv from 'dotenv';
import {Measure} from '../../models/Measure';
import {Utils} from '../../utils/Utils';
const jwt = require('jsonwebtoken');

dotenv.load();

interface AddMeasureParams {
    stationId: number;
    sensorId: number;
    value: number;
}

interface DecodedHeader {
    sensorId: number;
    iat: number;
    exp: number;
}

interface GenerateTokenParams {
    sensorId: number;
}

const valueZone = { bad: 3, warning: 2, good: 1};
const notificationStates = {
    bad: process.env.NOTIFICATIONBADMESSAGE,
    warning: process.env.NOTIFICATIONWARNINGMESSAGE,
    good: process.env.NOTIFICATIONGOODMESSAGE,
};

export class Mutations extends ObjectResolver implements Resolver {
    secret = process.env.JWTSECRET;
    tokenExpiration = '5y';

    private mutationResolver = {
        Mutation: {
            addMeasure: async (root: any, args: AddMeasureParams, context: any) => {
                const auth = context.headers.authorization;
                const decodedAuthorization: DecodedHeader = jwt.verify(auth, this.secret);
                if (decodedAuthorization) {
                    this.getDbClient().getMeasureBySensorId(args.sensorId).then((measures: Measure[]) => {
                        if (measures.length > 0 ) {
                            const previousZone = Utils.getColor(measures[0].value, valueZone);
                            const currentZone = Utils.getColor(args.value, valueZone);
                            if (previousZone != currentZone) {
                                const currentState = Utils.getColor(args.value, notificationStates);
                                const previousState = Utils.getColor(measures[0].value, notificationStates);
                                this.getDbClient().addNotification(args.stationId, measures[0].value, args.value, previousState, currentState);
                            }
                        }
                    });
                    return this.getDbClient().addMeasure(args.stationId, args.sensorId, args.value);
                }
            },

            generateSensorToken: async (root: any, args: GenerateTokenParams) => {
                const JWTToken = jwt.sign({
                    sensorId: args.sensorId,
                },
                this.secret,
                {
                    expiresIn: this.tokenExpiration,
                });
                return JWTToken;
            },
        },
    };

    public getResolver() {
        return this.mutationResolver;
    }
}
