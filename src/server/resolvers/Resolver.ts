import {DBClient} from '../DBClient';

export interface Resolver {
    getResolver(): any;
}

export abstract class ObjectResolver {

    constructor(private dbClient: DBClient) {
        this.dbClient = dbClient;
    }

    protected getDbClient() {
        return this.dbClient;
    }
}
