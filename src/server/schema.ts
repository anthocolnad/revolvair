import * as fs from 'fs';

const schemaFile = [
    './src/server/schema/Station.graphql',
    './src/server/schema/Sensor.graphql',
    './src/server/schema/Measure.graphql',
    './src/server/schema/Root.graphql',
    './src/server/schema/Notification.graphql',
];

export const schemas = schemaFile.map((file) => {
    return fs.readFileSync(file, 'utf-8');
});
