import * as elasticsearch from 'elasticsearch';
import * as esb from 'elastic-builder';
import * as moment from 'moment';
import * as dotenv from 'dotenv';

import {Sensor} from '../models/Sensor';
import {Notification} from '../models/Notification';
import {Measure} from '../models/Measure';
import {Station} from '../models/Station';
import {DBClient} from '../server/DBClient';
import {TimeRangeArgs} from './DBClient';

dotenv.load();

interface ElasticStationType {
    stationId: number;
    stationName: string;
    userId: number;
    location: {lon: number, lat: number };
}

interface ElasticMeasureType {
    stationId: number;
    '@timestamp': string;
    value: number;
    sensorId: number;
}

interface ElasticNotificationType {
    stationId: number;
    '@timestamp': string;
    newValue: number;
    oldValue: number;
    oldState: string;
    newState: string;
}

interface Bucket {
    key: string;
    doc_count: number;
}

export type MapFunction<T> = (resp: elasticsearch.SearchResponse<Bucket| ElasticStationType | ElasticNotificationType | ElasticMeasureType>) => T[];

const measuresIndexName = 'revolvairmeasures';
const measureType = 'measures';
const stationIndexName = 'revolvairstations';
const stationType = 'stations';
const notificationIndexName = 'revolvairnotifications';
const notificationType = 'notifications';

const maxSizeForStation = 300;
const maxSizeForMeasure = 300;
const sensorIdAggKey = 'stationIds';
const sensorIdKey = 'sensorId';
const stationIdKey = 'stationId';
const bucketsKey = 'buckets';
const timestampKey = '@timestamp';
const quebecZone = -5;

const notificationListSize = Number(process.env.NOTIFICATIONSLISTSIZE || 10);

export const elasticClient = new elasticsearch.Client({
    host: process.env.ELASTICHOST,
});

export class ElasticClientDB implements DBClient {

    elasticClient: elasticsearch.Client;

    constructor(elasticClient: elasticsearch.Client) {
        this.elasticClient = elasticClient;
    }

    ///////////////////////////
    //         GET           //
    ///////////////////////////
    public async getMeasureByTimeOption(sensorId: number, timeRangeArgs: TimeRangeArgs) {
        let lastLength: number = 0;
        let totalLength: number = 0;
        const measureList = new Array<Measure>();
        do {
            const query = new esb.RequestBodySearch().query(
                esb.boolQuery()
                    .must(
                        esb.matchQuery(sensorIdKey, String(sensorId)),
                    )
                .filter(esb.rangeQuery(timestampKey)
                    .gte(timeRangeArgs.timeRange)
                    .format('epoch_millis')),
            )
            .size(maxSizeForMeasure)
            .sort(esb.sort(timestampKey, 'desc'))
            .from(totalLength);
            const measures = await this.executeMeasuresQuery(query, this.mapMeasureFromResponse);
            measureList.push(...measures);
            lastLength = measures.length;
            totalLength += measures.length;
        } while ( lastLength != 0 );
        return measureList;
    }

    public async getAllStations() {
        let lastLength: number = 0;
        let totalLength: number = 0;

        const stationsList = new Array<Station>();
        do {
            const query = new esb.RequestBodySearch().query(
                new esb.MatchAllQuery(),
            )
            .size(maxSizeForStation)
            .from(totalLength);
            const stations = await this.executeStationQuery(query, this.mapStationFromResponse);
            stationsList.push(...stations);
            lastLength = stations.length;
            totalLength += stations.length;
        } while ( lastLength != 0 );
        return stationsList;
    }

    public async getAllStationById(id: number) {
        return await this.dbSearchStationByIdInStation(id, this.mapStationFromResponse);
    }

    public async getSensorsIdsByStationId(id: number) {
        return await this.dbSearchStationById(id, this.mapSensorIdFromBucketFromResponse);
    }

    public async getSensorIdByStationIdAndSensorId(stationId: number, sensorId: number) {
        return await this.dbSearchSensorAndStationById(stationId, sensorId, this.mapSensorIdFromBucketFromResponse);
    }

    public async getMeasureBySensorId(sensorId: number) {
        return await this.dbSearchSensorById(sensorId, this.mapMeasureFromResponse);
    }

    public async getNotificationByStationId(stationId: number) {
        const query = new esb.RequestBodySearch().query(
            esb.boolQuery()
                .must(esb.matchQuery(stationIdKey, String(stationId))))
            .size(notificationListSize)
            .sort(esb.sort(timestampKey, 'desc'));
        return await this.executeNotificationQuery(query, this.mapNotificationFromResponse);
    }

    ///////////////////////////
    //         ADD           //
    ///////////////////////////
    public async addMeasure(stationId: number, sensorId: number, value: number) {
        const measure = {
            '@timestamp': String(moment().utcOffset(quebecZone).unix() * 1000),
            stationId: stationId,
            sensorId: sensorId,
            value: value,
        };
        await this.indexMeasure(measure);
        return { value: value, sensorId: sensorId, timestamp: String(measure[timestampKey]) };
    }

    public async addNotification(stationId: number, oldValue: number, newValue: number, oldState: string, newState: string) {
        const notification = {
            '@timestamp': String(moment().utcOffset(quebecZone).unix() * 1000),
            stationId: stationId,
            oldValue: oldValue,
            newValue: newValue,
            oldState: oldState,
            newState: newState,
        };
        await this.indexNotification(notification);
    }

    private async indexNotification(notification: ElasticNotificationType) {
        await this.indexRessource(notificationIndexName, notificationType, notification);
    }

    private async indexMeasure(measure: ElasticMeasureType) {
        await this.indexRessource(measuresIndexName, measureType, measure);
    }

    private async indexRessource(indexName: string, indexType: string, body: ElasticMeasureType | ElasticNotificationType) {
        await this.elasticClient.index({
            index: indexName,
            type: indexType,
            body: body,
        });
    }

    private dbSearchSensorById(sensorId: number, mapFunction: MapFunction<any>) {
        const query = new esb.RequestBodySearch().query(
            esb.boolQuery()
                .must(esb.matchQuery(sensorIdKey, String(sensorId))))
            .sort(esb.sort(timestampKey, 'desc'));
        return this.executeMeasuresQuery(query, mapFunction);
    }

    private dbSearchSensorAndStationById(stationId: number, sensorId: number, mapFunction: MapFunction<any>) {
        const query = new esb.RequestBodySearch().query(
            esb.boolQuery()
                .must(esb.matchQuery(sensorIdKey, String(sensorId)))
                .must(esb.matchQuery(stationIdKey, String(stationId))))
            .agg(
                esb.termsAggregation(sensorIdAggKey, sensorIdKey),
            );
        return this.executeMeasuresQuery(query, mapFunction);
    }

    private dbSearchStationByIdInStation(stationId: number, mapFunction: MapFunction<any>) {
        const query = new esb.RequestBodySearch().query(
            esb.boolQuery()
                .must(esb.matchQuery(stationIdKey, String(stationId))));
        return this.executeStationQuery(query, mapFunction);
    }

    private dbSearchStationById(stationId: number, mapFunction: MapFunction<any>) {
        const query = new esb.RequestBodySearch().query(
            esb.boolQuery()
                .must(esb.matchQuery(stationIdKey, String(stationId))))
            .agg(
                esb.termsAggregation(sensorIdAggKey, sensorIdKey),
            );
        return this.executeMeasuresQuery(query, mapFunction);
    }

    private executeNotificationQuery(query: esb.RequestBodySearch, mapFunction: MapFunction<any>) {
        return this.executeQuery(notificationIndexName, notificationType, query, mapFunction);
    }

    private executeStationQuery(query: esb.RequestBodySearch, mapFunction: MapFunction<any>) {
        return this.executeQuery(stationIndexName, stationType, query, mapFunction);
    }

    private executeMeasuresQuery(query: esb.RequestBodySearch, mapFunction: MapFunction<any>) {
        return this.executeQuery(measuresIndexName, measureType, query, mapFunction);
    }

    private executeQuery(indexName: string, type: string, query: esb.RequestBodySearch, mapFunction: MapFunction<any>) {
        return this.elasticClient.search<any>({
            index: indexName,
            type: type,
            body: query.toJSON(),
        }).then(function(resp) {
            return mapFunction(resp);
        });
    }

    private mapSensorIdFromBucketFromResponse: MapFunction<Sensor> = (resp: elasticsearch.SearchResponse<Bucket>) => {
        return resp.aggregations[sensorIdAggKey][bucketsKey].map((bucket: Bucket) => {
            return {
                id: bucket.key,
            };
        });
    }

    private mapNotificationFromResponse: MapFunction<Notification> = (resp: elasticsearch.SearchResponse<ElasticNotificationType>) => {
        return resp.hits.hits.map(element => {
            return {
                stationId: element._source.stationId,
                oldValue: element._source.oldValue,
                newValue: element._source.newValue,
                timestamp: element._source[timestampKey],
                oldState: element._source.oldState,
                newState: element._source.newState,
            };
        });
    }
    private mapMeasureFromResponse: MapFunction<Measure> = (resp: elasticsearch.SearchResponse<ElasticMeasureType>) => {
        return resp.hits.hits.map(element => {
            return {
                sensorId: element._source.sensorId,
                value: element._source.value,
                timestamp: element._source[timestampKey],
            };
        });
    }

    private mapStationFromResponse: MapFunction<Station> = (resp: elasticsearch.SearchResponse<ElasticStationType>) => {
        return resp.hits.hits.map(element => {
            return {
                id: element._source.stationId,
                userId: element._source.userId,
                long: element._source.location.lon,
                lat: element._source.location.lat,
                name: element._source.stationName,
            };
        });
    }
}
