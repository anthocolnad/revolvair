import { SearchResponse } from 'elasticsearch';
import {Station} from '../models/Station';
import {Sensor} from '../models/Sensor';
import {Notification} from '../models/Notification';

jest.mock('elasticsearch');
import { Client } from 'elasticsearch';

import {
    ElasticClientDB,
} from './elastic';
import {Measure} from '../models/Measure';

const userId = 2;
const sensorId = 1;
const stationName = 'china';
const measureValue = 1;
const stationId = 3;
const long = 20;
const lat = 30;
const timestamp = '12:12:12';
const oldValue = 4;
const newValue = 9;
const state = 'bad';
const newState = 'warning';
const oldState = 'alert';

const value: SearchResponse<any> = {
    took: null,
    timed_out: null,
    _shards: null,
    hits: {
        total: 1,
        max_score: 1,
        hits: [{
            _index: 'test',
            _type: 'measure',
            _id: '1',
            _score: 1,
            _source: {
                sensorId: sensorId,
                userId: userId,
                location: {
                    lat: lat,
                    lon: long,
                },
                stationId: stationId,
                stationName: stationName,
                value: measureValue,
                '@timestamp': timestamp,
                oldValue: oldValue,
                newValue: newValue,
                state: state,
                newState: newState,
                oldState: oldState,
            },
        }],
    },
    aggregations: {
        stationIds: {
            doc_count_error_upper_bound: 0,
            sum_other_doc_count: 0,
            buckets: [ { key: sensorId, doc_count: 201 } ],
        },
    },
};

const valueEmptyList: SearchResponse<any> = {
    took: null,
    timed_out: null,
    _shards: null,
    hits: {
        total: 1,
        max_score: 1,
        hits: [],
    },
    aggregations: {
        stationIds: {
            doc_count_error_upper_bound: 0,
            sum_other_doc_count: 0,
            buckets: [ { key: sensorId, doc_count: 201 } ],
        },
    },
};

describe('When do db search', () => {
    let elasticClient: ElasticClientDB;
    let client: Client;
    beforeEach(() => {
        client =  new Client({host: '123'});
        (Client as jest.Mock<Client>).mockClear();
        client.search = jest.fn().mockImplementationOnce(fn => new Promise((resolve) => {
            resolve(value);
        })).mockImplementationOnce(fn => new Promise((resolve) => {
            resolve(valueEmptyList);
        }));

        elasticClient = new ElasticClientDB(client);
    });
    describe('getAllStations', () => {
        it('Should call elastic client', (done) => {
            elasticClient.getAllStations().then(() => {
                expect(client.search).toHaveBeenCalledTimes(2);
                done();
            });
        });
        it('Should return a list of stations', (done) => {
            const expectedStation = {
                id: stationId,
                name: stationName,
                userId: userId,
                lat: lat,
                long: long,
            };

            elasticClient.getAllStations().then((value: Station[]) => {
                expect(value[0].id).toBe(expectedStation.id);
                expect(value[0].name).toBe(expectedStation.name);
                expect(value[0].userId).toBe(expectedStation.userId);
                done();
            });
        });
    });
    describe('getAllStationById', () => {
        it('Should call elastic client', (done) => {
            elasticClient.getAllStationById(stationId).then(() => {
                expect(client.search).toHaveBeenCalledTimes(1);
                done();
            });
        });
        it('Should return a station', (done) => {
            const expectedStation = {
                id: stationId,
                name: stationName,
                userId: userId,
                lat: lat,
                long: long,
            };
            elasticClient.getAllStationById(stationId).then((value: Station[]) => {
                expect(value[0].id).toBe(expectedStation.id);
                expect(value[0].name).toBe(expectedStation.name);
                expect(value[0].userId).toBe(expectedStation.userId);
                expect(value[0].lat).toBe(expectedStation.lat);
                expect(value[0].long).toBe(expectedStation.long);
                done();
            });
        });
    });
    describe('getSensorsIdsByStationId', () => {
        it('Should call elastic client', (done) => {
            elasticClient.getSensorsIdsByStationId(stationId).then(() => {
                expect(client.search).toHaveBeenCalledTimes(1);
                done();
            });
        });
        it('Should return a list of sensors', (done) => {
            const expectedSensor = {
                stationid: stationId,
                id: sensorId,
            };
            elasticClient.getSensorsIdsByStationId(stationId).then((value: Sensor[]) => {
                expect(value[0].id).toBe(expectedSensor.id);
                done();
            });
        });
    });
    describe('getSensorIdByStationIdAndSensorId', () => {
        it('Should call elastic client', (done) => {
            elasticClient.getSensorIdByStationIdAndSensorId(stationId, sensorId).then(() => {
                expect(client.search).toHaveBeenCalledTimes(1);
                done();
            });
        });
        it('Should return a sensor', (done) => {
            const expectedSensor = {
                stationid: stationId,
                id: sensorId,
            };
            elasticClient.getSensorIdByStationIdAndSensorId(stationId, stationId).then((value: Sensor[]) => {
                expect(value[0].id).toBe(expectedSensor.id);
                done();
            });
        });
    });
    describe('getMeasureBySensorId', () => {
        it('Should leave infinite loop', (done) => {
            elasticClient.getMeasureBySensorId(sensorId).then(() => {
                expect(client.search).toHaveBeenCalledTimes(1);
                done();
            });
        });
        it('Should return a list of measures', (done) => {
            const expectedSensor = {
                sensorId: sensorId,
                value: measureValue,
                timestamp: timestamp,
            };
            elasticClient.getMeasureBySensorId(stationId).then((value: Measure[]) => {
                expect(value[0].value).toBe(expectedSensor.value);
                expect(value[0].timestamp).toBe(expectedSensor.timestamp);
                done();
            });
        });
    });
    describe('getMeasureByTimeOption', () => {
        it('Should leave infinite loop', (done) => {
            elasticClient.getMeasureByTimeOption(sensorId, {timeRange: 'now-1d'}).then(() => {
                expect(client.search).toHaveBeenCalledTimes(2);
                done();
            });
        });
    });
    describe('getNotificationByStationId', () => {
        it('Should call elastic client', () => {
            elasticClient.getNotificationByStationId(stationId).then(() => {
                expect(client.search).toHaveBeenCalledTimes(1);
            });
        });
        it('Should return notification', () => {
            elasticClient.getNotificationByStationId(stationId).then((notification: Notification[]) => {
                const actualNotification = notification[0];
                expect(actualNotification.stationId).toBe(stationId);
                expect(actualNotification.newValue).toBe(newValue);
                expect(actualNotification.oldValue).toBe(oldValue);
                expect(actualNotification.timestamp).toBe(timestamp);
                expect(actualNotification.oldState).toBe(oldState);
                expect(actualNotification.newState).toBe(newState);
            });
        });
    });
});

describe('When do db search and elastic return empty list', () => {
    let elasticClient: ElasticClientDB;
    let client: Client;
    beforeEach(() => {
        client =  new Client({host: '123'});
        (Client as jest.Mock<Client>).mockClear();
        client.search = jest.fn().mockReturnValue(new Promise((resolve) => {
            resolve(valueEmptyList);
        }));

        client.index = jest.fn().mockReturnValue(new Promise((resolve) => {
            resolve(valueEmptyList);
        }));
        elasticClient = new ElasticClientDB(client);
    });

    describe('getMeasureByTimeOption', () => {
        it('Should call elastic client', (done) => {
            elasticClient.getMeasureByTimeOption(sensorId, {timeRange: 'now-1d'}).then(() => {
                expect(client.search).toHaveBeenCalledTimes(1);
                done();
            });
        });
        it('Should call elastic client', (done) => {
            elasticClient.getMeasureBySensorId(sensorId).then(() => {
                expect(client.search).toHaveBeenCalledTimes(1);
                done();
            });
        });
    });
    describe('addMeasure', () => {
        it('Should call elastic client', (done) => {
            elasticClient.addMeasure(stationId, sensorId, 1234).then(() => {
                expect(client.index).toHaveBeenCalledTimes(1);
                done();
            });
        });
    });
});
