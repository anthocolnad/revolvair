import * as express from 'express';
import {createServer} from 'http';
import * as moment from 'moment';
import * as React from 'react';
import * as ReactDOMServer from 'react-dom/server';
import { GraphQLSchema } from 'graphql';
import {makeExecutableSchema} from 'graphql-tools';
import * as rateLimit from 'express-rate-limit';

import * as dotenv from 'dotenv';

import * as graphqlHTTP from 'express-graphql';
import { resolvers } from './server/resolvers';
import { schemas } from './server/schema';

moment.locale('fr');
dotenv.config();

const throttlingTimeGap = 15 * 60 * 1000;
const maxCall = 100;

try {
    const app = express();
    createServer(app).listen(process.env.PORT || 3000);

    app.use(new rateLimit({
        windowMs: throttlingTimeGap,
        max: maxCall,
    }));

    const schema: GraphQLSchema = makeExecutableSchema({
        typeDefs: schemas,
        resolvers: resolvers,
    });

    app.use('/graphql',
            graphqlHTTP(req => ({
                schema,
                graphiql: true,
            })));

    app.use(express.static('dist/public'));
    app.get('/*', (req, res, next) => {
        res.send(ReactDOMServer.renderToString(
            <html>
                <head>
                    <title>RevolvAir : Technologies pour un air pur</title>
                    <meta charSet='utf8'/>
                    <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' />
                    <link rel='stylesheet' href='//cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css'/>
                    <link rel='stylesheet' href='//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css'/>
                    <link rel='stylesheet' href='/css/styles.bundle.css'/>
                    <script src='//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js'></script>
                </head>
                <body>
                    <main id='reactroot' className='revolvair'/>
                </body>
                <script src='/js/frontend.bundle.js'></script>
            </html>,
        ));
    });
} catch (error) {
    console.log(error);
    process.exit();
}
