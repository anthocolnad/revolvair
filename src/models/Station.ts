import {Sensor} from './Sensor';
import {Notification} from './Notification';

export interface Station {
    id: number;
    name: string;
    long: number;
    lat: number;
    userId?: number;
    sensors?: Sensor[];
    notifications?: Notification[];
}
