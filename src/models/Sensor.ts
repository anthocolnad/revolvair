import {Measure} from './Measure';

export interface Sensor {
    id: number;
    stationId: number;
    measures?: Measure[];
    latestMeasures?: Measure[];
    max?: number;
    min?: number;
    avg?: number;
}
