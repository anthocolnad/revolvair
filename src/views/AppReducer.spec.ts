import {revolvairReducer, defaultRevolvairState} from './AppReducer';

describe('RevolvairReducer', () => {
    it('should return default state if it undefined', () => {
        expect(revolvairReducer(undefined, {type: undefined})).toEqual(defaultRevolvairState);
    });
});
