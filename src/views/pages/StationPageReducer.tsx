import {Reducer} from 'redux';
import {StationPageActions, StationPageAllAction} from './StationPageActions';
import {Station} from '../../models/Station';

export interface StationPageState {
    station: Station;
}

export const defaultStationPageState: StationPageState = {
    station: undefined,
};

export const StationPageReducer: Reducer<StationPageState, StationPageAllAction> = (state = defaultStationPageState, action) => {
    switch (action.type) {
        case StationPageActions.GET_STATION:
            return {...state, station: action.station};
        default:
            return state;
    }
};
