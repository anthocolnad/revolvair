import {Action} from 'redux';
import {Point} from '../components/GoogleMap';
import {orderDirection} from '../components/TableComponent';
import {Station} from '../../models/Station';

export enum HomePageActions {
    UPDATE_MAP_LOCATION = 'UPDATE_MAP_LOCATION',
    UPDATE_MAP_ZOOM = 'UPDATE_MAP_ZOOM',
    UPDATE_ORDER_TABLE = 'UPDATE_ORDER_TABLE',
    GET_STATIONS = 'GET_STATIONS',
}

export type HomePageAction = Action<HomePageActions>;
export type HomePageAllAction = Partial<HomePageUpdateLocationAction & HomePageUpdateZoomAction & HomePageUpdateOrderAction & HomePageGetStationAction> & HomePageAction;
export type HomePageUpdateLocationAction = HomePageAction & {location: Point};
export type HomePageUpdateZoomAction = HomePageAction & {zoom: number};
export type HomePageUpdateOrderAction = HomePageAction & {order: orderDirection, orderRow: number};
export type HomePageGetStationAction = HomePageAction & {stations: Station[]};


export function updateLocation(location: Point): HomePageUpdateLocationAction {
    return {
        type: HomePageActions.UPDATE_MAP_LOCATION,
        location,
    };
}

export function updateZoom(zoom: number): HomePageUpdateZoomAction {
    return {
        type: HomePageActions.UPDATE_MAP_ZOOM,
        zoom,
    };
}

export function updateOrder(order: orderDirection, orderRow: number): HomePageUpdateOrderAction {
    return {
        type: HomePageActions.UPDATE_ORDER_TABLE,
        order,
        orderRow,
    };
}

export function getStations(stations: Station[]): HomePageGetStationAction {
    return {
        type: HomePageActions.GET_STATIONS,
        stations,
    };
}
