import {TableCell, TableRow} from '@material-ui/core';
import Badge from '@material-ui/core/Badge';
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress';
import Grid from '@material-ui/core/Grid/Grid';
import {NormalizedCacheObject} from 'apollo-cache-inmemory';
import ApolloClient from 'apollo-client';
import gql from 'graphql-tag';
import * as moment from 'moment';
import * as React from 'react';
import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import {isDefined, isNotEmpty} from 'welshguard';
import {Station} from '../../models/Station';
import {Formatter} from '../../utils/Formatter';
import {Utils} from '../../utils/Utils';
import {RevolvairState} from '../AppReducer';
import {Graph, GraphData, ImportantValues} from '../components/Graph';
import {ImportantValue} from '../components/ImportantValue';
import {SlideMenu} from '../components/SlideMenu';
import {TableComponent} from '../components/TableComponent';
import {HomePage} from './HomePage';
import {getStation} from './StationPageActions';


interface StationGraphData {
    data: GraphData[];
    importantValues: ImportantValues;
}

export interface StationPageProps extends StationPageStateToProps, StationPageDispatchToProps {
    stationId: number;
    apolloClient: ApolloClient<NormalizedCacheObject>;
}

interface StationPageStateToProps {
    station: Station;
}

const mapStateToProps = ({stationPage}: RevolvairState): StationPageStateToProps => ({
    station: stationPage.station,
});

interface StationPageDispatchToProps {
    getStation: (station: Station) => void;
}

const mapDispatchToProps = (dispatch: Dispatch): StationPageDispatchToProps => ({
    getStation: (station: Station) => dispatch(getStation(station)),
});

const GET_STATION = gql`
query station($id: Int!) {
    station(id: $id) {
        name
        sensors {
            measures(timeRange: "now-30d") {
                value
                timestamp
            }
        }
        notifications {
            oldValue
            newValue
            timestamp
            oldState
            newState
        }
    }
}`;

export class StationPage extends React.Component<StationPageProps> {
    public static DataKey: string = Formatter.DataKey;
    public static DATE_FORMAT_NOTIFICATION = 'Do MMMM YYYY, H:mm ';

    private getStationData(): void {
        this.props.apolloClient.cache.reset();
        this.props.apolloClient.query<{station: Station[]}>({query: GET_STATION, variables: {id: +this.props.stationId}})
            .then(({data}) => this.props.getStation(data.station[0]));
    }

    componentDidMount(): void {
        this.getStationData();
        setInterval(() => {
            this.getStationData();
        }, 30000);

    }
    public render(): React.ReactNode {
        return (
            <div className='station-page'>
                {isDefined(this.props.station)
                ? (
                    <div>
                        <Grid container justify='center' direction='column' spacing={0}>
                            {this.props.station.sensors && isNotEmpty(this.props.station.sensors)
                                ? this.getGraph(this.getDataList())
                                : (
                                    <Grid item xs={11} sm={8}>
                                        <p>Il n'y a pas de capteurs sur cette station.</p>
                                    </Grid>
                                )}
                        </Grid>
                        {isDefined(this.props.station.notifications)
                            ? (
                                <Grid>
                                    <h2 className='pb2'>Notifications</h2>
                                    <TableComponent
                                        className='notifications-table mb2'
                                        headTable={[
                                            {title: 'Date'},
                                            {title: 'Statut'},
                                            {title: 'Ancienne valeur'},
                                            {title: 'Nouvelle valeur'},
                                        ]}
                                        itemTable={this.props.station.notifications}
                                        rowRender={(notification) => {
                                            return (
                                                <TableRow className='table-row' key={notification.stationId + notification.timestamp}>
                                                    <TableCell>
                                                        {Formatter.formatDateNotification(StationPage.DATE_FORMAT_NOTIFICATION, +notification.timestamp)}
                                                    </TableCell>
                                                    <TableCell>
                                                        {`La valeur est passée de ${notification.oldState} à ${notification.newState}`}
                                                    </TableCell>
                                                    <TableCell>
                                                        <Badge classes={{
                                                            badge: Utils.getColor(notification.oldValue, {
                                                                bad: HomePage.RED_COLOR_BACKGROUND,
                                                                warning: HomePage.YELLOW_COLOR_BACKGROUND,
                                                                good: HomePage.GREEN_COLOR_BACKGROUND,
                                                        })}}>
                                                            {notification.oldValue}
                                                        </Badge>
                                                    </TableCell>
                                                    <TableCell>
                                                        <Badge classes={{
                                                            badge: Utils.getColor(notification.newValue, {
                                                                bad: HomePage.RED_COLOR_BACKGROUND,
                                                                warning: HomePage.YELLOW_COLOR_BACKGROUND,
                                                                good: HomePage.GREEN_COLOR_BACKGROUND,
                                                        })}}>
                                                            {notification.newValue}
                                                        </Badge>
                                                    </TableCell>
                                                </TableRow>
                                            );
                                        }}
                                    />
                                </Grid>
                            ) : (
                                <CircularProgress color='secondary'/>
                            )
                        }
                    </div>
                )
                : (
                    <CircularProgress color='secondary'/>
                )}
            </div>
        );
    }

    private getGraph(dataList: StationGraphData[]): React.ReactNode {
        return (
            <SlideMenu
                stationName={this.props.station.name}
                listName={['Aujourd\'hui', 'Dernière semaine', 'Dernier mois']}
                menuData={dataList}
                tabContainer={(data) => (
                    <div className='flex-align-center tabContainer pb2' key={JSON.stringify(data)}>
                        <div>
                            <Graph
                                dataDescription='Particules de combustion, composé organique, métaux, etc.'
                                width={1400}
                                title='PM2.5'
                                data={data.data}
                                dataKeyX='name'
                                dataKeyY={StationPage.DataKey}
                                colorLine='#7EDCF7'
                            />
                        </div>
                        <div className='station-page-important-values flex-between'>
                            <ImportantValue id='maximum' name='Maximum' value={data.importantValues.max} className='mx1'/>
                            <ImportantValue id='minimum' name='Minimum' value={data.importantValues.min} className='mx1'/>
                            <ImportantValue id='average' name='Moyenne' value={data.importantValues.avg} className='mx1'/>
                        </div>
                    </div>
                )}
            />
        );
    }

    private getDataList(): StationGraphData[] {
        const now = moment().endOf('day');
        const allData: Formatter.Info[] = Formatter.formatMeasures(this.props.station.sensors[0].measures);

        const days = Formatter.formatGraphData(Utils.TimeGap.Day, allData, 'H[h]', now, true);
        const weeks = Formatter.formatGraphData(Utils.TimeGap.Week, allData, 'M/D', now, true);
        const months = Formatter.formatGraphData(Utils.TimeGap.Month, allData, 'M/D', now, true);
        return [
            {
                data: days,
                importantValues: Formatter.formatImportantValuesFromGraphData(days.map(day => day.tooltip)),
            },
            {
                data: weeks,
                importantValues: Formatter.formatImportantValuesFromGraphData(weeks.map(week => week.tooltip)),
            },
            {
                data: months,
                importantValues: Formatter.formatImportantValuesFromGraphData(months.map(month => month.tooltip)),
            },
        ];
    }
}

export const StationPageConnected = connect(mapStateToProps, mapDispatchToProps)(StationPage);
