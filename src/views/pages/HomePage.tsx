import {TableCell, Tooltip} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import TableRow from '@material-ui/core/TableRow/TableRow';
import Icon from '@material-ui/icons/Place';
import Badge from '@material-ui/core/Badge';
import * as React from 'react';
import {connect} from 'react-redux';
import {RouteComponentProps, StaticContext, withRouter} from 'react-router';
import {Dispatch} from 'redux';
import {isDefined} from 'welshguard';
import {Station} from '../../models/Station';
import {Utils} from '../../utils/Utils';
import {RevolvairState} from '../AppReducer';
import {GoogleMap, Point} from '../components/GoogleMap';
import {orderDirection, TableComponent} from '../components/TableComponent';
import {updateLocation, updateOrder, updateZoom, getStations} from './HomePageActions';
import _ = require('underscore');
import {Dashboard} from '../applications/Dashboard';
import path = require('path');
import gql from 'graphql-tag';
import ApolloClient from 'apollo-client';
import {NormalizedCacheObject} from 'apollo-cache-inmemory';

interface HomePageStateToProps {
    geoLocation: Point;
    zoom: number;
    order: orderDirection;
    orderRow: number;
    stations: Station[];
}

export interface HomePageProps extends RouteComponentProps<any, StaticContext, any>, HomePageDispatchToProps, HomePageStateToProps {
    apolloClient: ApolloClient<NormalizedCacheObject>;
}

const mapStateToProps = ({homePage}: RevolvairState): HomePageStateToProps => ({
    geoLocation: homePage.location,
    zoom: homePage.zoom,
    order: homePage.order,
    orderRow: homePage.orderRow,
    stations: homePage.stations,
});

interface HomePageDispatchToProps {
    updateLocation: () => void;
    updateMapOnClick: (lat: number, lng: number, zoom: number) => void;
    updateOrderTable: (order: orderDirection, orderRow: number) => void;
    getStations: (stations: Station[]) => void;
}

const mapDispatchToProps = (dispatch: Dispatch): HomePageDispatchToProps => ({
    updateLocation: () => navigator.geolocation.getCurrentPosition(({coords}) => dispatch(updateLocation({lat: coords.latitude, lng: coords.longitude}))),
    updateMapOnClick: (lat: number, lng: number, zoom: number) => {
        dispatch(updateLocation({lat: lat, lng: lng}));
        dispatch(updateZoom(zoom));
    },
    updateOrderTable: (order: orderDirection, orderRow: number) => {
        dispatch(updateOrder(order, orderRow));
    },
    getStations: (stations: Station[]) => dispatch(getStations(stations)),
});

const GET_STATIONS = gql`
{
    stations {
        id
        name
        lat
        long
        sensors {
            latestMeasures {
                value
            }
        }
    }
}`;



export class HomePage extends React.Component<HomePageProps> {
    public static GREEN_COLOR = '#10a300';
    public static YELLOW_COLOR = 'yellow';
    public static RED_COLOR = 'red';
    public static BLUE_COLOR = '#64c2ef';

    public static GREEN_COLOR_BACKGROUND = 'green-back';
    public static YELLOW_COLOR_BACKGROUND = 'yellow-back';
    public static RED_COLOR_BACKGROUND = 'red-back';
    public static BLUE_COLOR_BACKGROUND = 'blue-back';

    private getStationList(): void {
        this.props.apolloClient.cache.reset();
        this.props.apolloClient.query<HomePageProps>({query: GET_STATIONS})
            .then(result => this.props.getStations(
                result.data.stations,
            ));
    }

    public constructor(props: HomePageProps) {
        super(props);
    }

    public componentDidMount(): void {
        this.props.updateLocation();
        this.getStationList();
        setInterval(() => {
            this.getStationList();
        }, 30000);

    }

    public render(): React.ReactNode {
        return (
            <div className='home-page'>
                <div className='interactive-map'>
                    <GoogleMap
                        zoom={this.props.zoom}
                        center={this.props.geoLocation}
                        onMarkerClick={(marker) => this.checkIfStationHasValue(marker.id) ?
                            this.props.history.push(path.join(Dashboard.STATION_PAGE_ON_CLICK, `${marker.id}`))
                            : null}
                        markers={_.map(
                            this.props.stations,
                            (station) => ({
                                id: station.id,
                                name: station.name,
                                value: `${Utils.getLastMeasure(station) || 'N\/A'}`,
                                location: {lat: station.lat, lng: station.long},
                            }),
                        )}
                    />
                </div>
                <TableComponent
                    defaultOrderDirection={this.props.order}
                    defaultOrderRow={this.props.orderRow}
                    headTable={[
                        {title: 'Stations', isSortable: true, onClick: (orderDirection, row) => this.props.updateOrderTable(orderDirection, row)},
                        {title: 'Dernière valeur PM2.5', isSortable: true, onClick: (orderDirection, row) => this.props.updateOrderTable(orderDirection, row)},
                        {title: 'Localisation', isSortable: false},
                    ]}
                    itemTable={this.props.stations}
                    onSort={(rows) => this.sortStation(rows, this.props.order, this.props.orderRow)}
                    rowRender={(station) => {
                        const lastMeasure = Utils.getLastMeasure(station);
                        return (
                            <Tooltip key={station.id} title={station.name}>
                                <TableRow
                                    className='table-row'
                                    onClick={isDefined(lastMeasure) ? (event) => {
                                        const url = path.join(Dashboard.STATION_PAGE_ON_CLICK, `${station.id}`);
                                        this.props.history.push(url);
                                    } : null}
                                >
                                    <TableCell>
                                        {station.name}
                                    </TableCell>
                                    {isDefined(lastMeasure)
                                    ? (
                                        <TableCell>
                                            <Badge classes={{
                                                badge: Utils.getColor(lastMeasure, {
                                                    bad: HomePage.RED_COLOR_BACKGROUND,
                                                    warning: HomePage.YELLOW_COLOR_BACKGROUND,
                                                    good: HomePage.GREEN_COLOR_BACKGROUND,
                                                }),
                                            }}>
                                            {lastMeasure}
                                            </Badge>
                                        </TableCell>
                                    )
                                    : (
                                        <TableCell>
                                           <Badge classes={{
                                               badge: HomePage.BLUE_COLOR_BACKGROUND,
                                           }}>
                                            N/A
                                           </Badge>
                                        </TableCell>
                                    )}
                                    <TableCell>
                                        <IconButton color={'secondary'} onClick={(event) => {
                                                this.props.updateMapOnClick(station.lat, station.long, 15);
                                                event.stopPropagation();
                                                event.preventDefault();
                                            }}>
                                            <Icon/>
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            </Tooltip>
                        );
                    }}
                />
            </div>
        );
    }
    private checkIfStationHasValue(id: number): boolean {
        const station = _.findWhere(this.props.stations, {id});
        if (station.sensors.length > 0) {
            return true;
        } else {
            return false;
        }
    }

    private sortStation(station: Station[], direction: orderDirection, row: number): Station[] {
        if (row === 0) {
            if (direction === TableComponent.ASCENDANT_SORT) {
                return station.sort((one, two) => (one.name < two.name ? -1 : 1));
            } else {
                return station.sort((one, two) => (one.name > two.name ? -1 : 1));
            }
        } else {
            if (direction === TableComponent.ASCENDANT_SORT) {
                return station.sort((one, two) => (
                Utils.getLastMeasure(one)
                < Utils.getLastMeasure(two) ? -1 : 1));
            } else {
                return station.sort((one, two) => (
                    Utils.getLastMeasure(one)
                > Utils.getLastMeasure(two) ? -1 : 1));
            }
        }
    }
}

export const HomePageWithRouter = withRouter(HomePage);
export const HomePageConnected = connect(mapStateToProps, mapDispatchToProps)(HomePageWithRouter);
