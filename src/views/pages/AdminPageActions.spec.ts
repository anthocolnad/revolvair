import {AdminPageActions, updateToken} from './AdminPageActions';

describe('AdminPageActions', () => {
    it('GENERATE_TOKEN should equal GENERATE_TOKEN', () => {
        expect(AdminPageActions.GENERATE_TOKEN).toBe('GENERATE_TOKEN');
    });

    describe('updateToken', () => {
        it('Should return an action with the token pass as params', () => {
            const token = 'token';
            const action = updateToken(token);
            expect(action.type).toBe(AdminPageActions.GENERATE_TOKEN);
            expect(action.token).toEqual(token);
        });
    });
});

