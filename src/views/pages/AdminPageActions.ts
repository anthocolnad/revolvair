import {Action} from 'redux';

export enum AdminPageActions {
    GENERATE_TOKEN = 'GENERATE_TOKEN',
}

export type AdminPageAction = Action<AdminPageActions>;
export type AdminPageAllAction = Partial<AdminPageUpdateTokenAction> & AdminPageAction;
export type AdminPageUpdateTokenAction = {type: AdminPageActions} & {token: string};

export function updateToken(token: string): AdminPageUpdateTokenAction {
    return {
        type: AdminPageActions.GENERATE_TOKEN,
        token,
    };
}
