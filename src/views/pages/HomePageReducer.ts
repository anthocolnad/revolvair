import {Reducer} from 'redux';
import {Point} from '../components/GoogleMap';
import {HomePageActions, HomePageAllAction} from './HomePageActions';
import {orderDirection, TableComponent} from '../components/TableComponent';
import {Station} from '../../models/Station';

export interface HomePageState {
    location: Point;
    zoom: number;
    order: orderDirection;
    orderRow: number;
    stations: Station[];
}

export const defaultHomePageState: HomePageState = {
    location: {lat: 46.8138, lng: -71.2079},
    zoom: 11,
    order: TableComponent.ASCENDANT_SORT,
    orderRow: 0,
    stations: [],
};

export const HomePageReducer: Reducer<HomePageState, HomePageAllAction> = (state = defaultHomePageState, action) => {
    switch (action.type) {
        case HomePageActions.UPDATE_MAP_LOCATION:
            return {...state, location: action.location};
        case HomePageActions.UPDATE_MAP_ZOOM:
            return {...state, zoom: action.zoom};
        case HomePageActions.UPDATE_ORDER_TABLE:
            return {...state, order: action.order, orderRow: action.orderRow};
        case HomePageActions.GET_STATIONS:
            return {...state, stations: action.stations};
        default:
            return state;
    }
};
