import * as React from 'react';
import Button from '@material-ui/core/Button';
import {NormalizedCacheObject} from 'apollo-cache-inmemory';
import {Dispatch} from 'redux';
import gql from 'graphql-tag';
import ApolloClient from 'apollo-client';
import {updateToken} from './AdminPageActions';
import {RevolvairState} from '../AppReducer';
import {connect} from 'react-redux';

interface AdminPageProps extends AdminPageDispatchToProps, AdminPageStateToProps {
    apolloClient: ApolloClient<NormalizedCacheObject>;
}

interface GenerateTokenResponse {
    data: {
        generateSensorToken: string;
    };
}

interface AdminPageStateToProps {
    token: string;
}

const mapStateToProps = ({adminPage}: RevolvairState): AdminPageStateToProps => ({
    token: adminPage.token,
});

interface AdminPageDispatchToProps {
    updateToken: (token: string) => void;
}

const mapDispatchToProps = (dispatch: Dispatch): AdminPageDispatchToProps => ({
    updateToken: (token: string) => dispatch(updateToken(token)),
});

const GENERATE_TOKEN = gql`
    mutation {
        generateSensorToken(sensorId: 2)
    }
`;

export class AdminPage extends React.Component<AdminPageProps> {

    private runQuery(): void {
        this.props.apolloClient.mutate({
            mutation: GENERATE_TOKEN,
        }).then((data: GenerateTokenResponse) => {
            this.props.updateToken(data.data.generateSensorToken);
        });
    }

    public render(): React.ReactNode {
        return (
            <div className='admin-page'>
                <Button variant='contained' onClick={() => this.runQuery()}>
                    générer un token
                </Button>
                <p className='pt3'>{this.props.token}</p>
            </div>
        );
    }
}

export const AdminPageConnected = connect(mapStateToProps, mapDispatchToProps)(AdminPage);
