import {Reducer} from 'redux';
import {DashboardAllAction} from '../views/applications/DashboardActions';
import {DashboardReducer, DashboardState, defaultDashboardState} from './applications/DashboardReducer';
import {defaultHomePageState, HomePageState, HomePageReducer} from './pages/HomePageReducer';
import {HomePageAllAction} from './pages/HomePageActions';
import {AdminPageState, defaultAdminPageState, AdminPageReducer} from './pages/AdminPageReducer';
import {AdminPageAllAction} from './pages/AdminPageActions';
import {defaultStationPageState, StationPageReducer, StationPageState} from './pages/StationPageReducer';
import {StationPageAllAction} from './pages/StationPageActions';

export interface RevolvairState {
    dashboard: DashboardState;
    homePage: HomePageState;
    adminPage: AdminPageState;
    stationPage: StationPageState;
}

export const defaultRevolvairState: RevolvairState = {
    dashboard: defaultDashboardState,
    homePage: defaultHomePageState,
    adminPage: defaultAdminPageState,
    stationPage: defaultStationPageState,
};

type RevolvairAllAction = DashboardAllAction & HomePageAllAction & AdminPageAllAction & StationPageAllAction;

export const revolvairReducer: Reducer<RevolvairState, RevolvairAllAction> = (state = defaultRevolvairState, action) => ({
    dashboard: DashboardReducer(state.dashboard, action),
    homePage: HomePageReducer(state.homePage, action),
    adminPage: AdminPageReducer(state.adminPage, action),
    stationPage: StationPageReducer(state.stationPage, action),
});
