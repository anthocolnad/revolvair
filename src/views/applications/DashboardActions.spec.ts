import {Station} from '../../models/Station';
import {updateStations, DashboardActions, changeDrawer} from './DashboardActions';

describe('DashboardActions', () => {
    describe('updateStations', () => {
        it('Should return stations list if it passes as attribute.', () => {
            const array: Station[] = [{id: 0, name: 'name', long: 1, lat: 1, sensors: []}];

            expect(updateStations(array).type).toBe(DashboardActions.UPDATE_STATIONS);
            expect(updateStations(array).stations).toEqual([...array]);
        });
    });
    describe('changeDrawer', () => {
        it('Should return drawer if it passes as attribute.', () => {
            const drawer: boolean = true;

            expect(changeDrawer(drawer).type).toBe(DashboardActions.CHANGE_DRAWER);
            expect(changeDrawer(drawer).drawer).toEqual(drawer);
        });
    });
});
