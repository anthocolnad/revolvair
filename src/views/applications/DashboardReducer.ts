import {Reducer} from 'redux';
import {Station} from '../../models/Station';
import {DashboardActions, DashboardAllAction} from './DashboardActions';

export interface DashboardState {
    stations: Station[];
    drawer: boolean;
}

export const defaultDashboardState: DashboardState = {
    stations: [],
    drawer: false,
};

export const DashboardReducer: Reducer<DashboardState, DashboardAllAction> = (state = defaultDashboardState, action) => {
    switch (action.type) {
        case DashboardActions.UPDATE_STATIONS:
            return {...state, stations: action.stations};
        case DashboardActions.CHANGE_DRAWER:
            return {...state, drawer: action.drawer};
        default:
            return state;
    }
};
