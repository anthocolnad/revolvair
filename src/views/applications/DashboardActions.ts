import {Action} from 'redux';
import {Station} from '../../models/Station';

export enum DashboardActions {
    UPDATE_STATIONS = 'UPDATE_STATIONS',
    CHANGE_DRAWER = 'CHANGE_DRAWER',
}

export type DashboardAction = Action<DashboardActions>;
export type DashboardAllAction = Partial<DashboardStationAction & DashboardChangeDrawerAction> & DashboardAction;
export type DashboardStationAction = DashboardAction & {stations: Station[]};
export type DashboardChangeDrawerAction = DashboardAction & {drawer: boolean};

export const updateStations = (stations: Station[]): DashboardStationAction => ({
    type: DashboardActions.UPDATE_STATIONS,
    stations,
});

export const changeDrawer = (drawer: boolean): DashboardChangeDrawerAction => ({
    type: DashboardActions.CHANGE_DRAWER,
    drawer,
});
