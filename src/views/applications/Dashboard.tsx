import {InMemoryCache, NormalizedCacheObject} from 'apollo-cache-inmemory';
import ApolloClient from 'apollo-client';
import {HttpLink} from 'apollo-link-http';
import {Location} from 'history';
import * as React from 'react';
import {connect} from 'react-redux';
import {Redirect, Route, Switch} from 'react-router';
import {Dispatch} from 'redux';
import {Station} from '../../models/Station';
import {RevolvairState} from '../AppReducer';
import {FooterBarWithRouter} from '../components/FooterBar';
import {NavBarWithRouter} from '../components/NavBar';
import {AboutUsPageWithRouter} from '../pages/AboutUsPage';
import {AdminPageConnected} from '../pages/AdminPage';
import {EmailSubscriptionPage} from '../pages/EmailSubscription';
import {HomePageConnected} from '../pages/HomePage';
import {StationPageConnected} from '../pages/StationPage';
import path = require('path');

interface DashboardStateToProps {
    stations: Station[];
}

const mapStateToProps = ({dashboard}: RevolvairState): DashboardStateToProps => ({
    stations: dashboard.stations,
});

interface DashboardDispatchToProps {
}

const mapDispatchToProps = (dispatch: Dispatch): DashboardDispatchToProps => ({
});

export interface DashboardProps extends DashboardDispatchToProps, DashboardStateToProps {
    location: Location<any>;
}

export class Dashboard extends React.Component<DashboardProps> {
    public static ROOT_PAGE = '/dashboard';
    public static HOME_PAGE = path.join(Dashboard.ROOT_PAGE, 'home');
    public static STATION_PAGE = path.join(Dashboard.ROOT_PAGE, 'station/:id');
    public static STATION_PAGE_ON_CLICK = path.join(Dashboard.ROOT_PAGE, 'station');
    public static INFO_PAGE = path.join(Dashboard.ROOT_PAGE, 'aboutus');
    public static ADMIN_PAGE = path.join(Dashboard.ROOT_PAGE, 'admin');
    public static SUBSCRIPTION_PAGE = path.join(Dashboard.ROOT_PAGE, 'subscription');

    private client: ApolloClient<NormalizedCacheObject> = new ApolloClient({
        link: new HttpLink(),
        cache: new InMemoryCache(),
    });

    private updateStationList(): void {
        this.client.cache.reset();
    }

    public componentDidMount() {
        setInterval(() => {
            this.updateStationList();
        }, 30000);
    }

    public render(): React.ReactNode {
        return (
            <div className='flex-col flex-between'>
                <div>
                    <NavBarWithRouter/>
                    <div className='p2 flex limit-x'>
                        <div className='full-width'>
                            <Switch>
                                <Route
                                    exact
                                    path={Dashboard.HOME_PAGE}
                                    render={() => <HomePageConnected apolloClient={this.client}/>}
                                />
                                <Route
                                    exact
                                    path={Dashboard.STATION_PAGE}
                                    render={({match}) => (
                                        <StationPageConnected stationId={match.params.id} apolloClient={this.client}/>
                                    )}
                                />
                                <Route
                                    exact
                                    path={Dashboard.ADMIN_PAGE}
                                    render={() => <AdminPageConnected apolloClient={this.client}/>}
                                />
                                <Route
                                    exact
                                    path={Dashboard.INFO_PAGE}
                                    render={() => <AboutUsPageWithRouter/>}
                                />
                                <Route
                                    exact
                                    path={Dashboard.SUBSCRIPTION_PAGE}
                                    render={() => <EmailSubscriptionPage/>}
                                />
                                <Redirect to={Dashboard.HOME_PAGE}/>
                            </Switch>
                        </div>
                    </div>
                </div>
                <FooterBarWithRouter/>
            </div>
        );
    }
}

export const DashboardConnected = connect(mapStateToProps, mapDispatchToProps)(Dashboard);
