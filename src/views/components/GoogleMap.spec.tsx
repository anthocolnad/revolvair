import * as Enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';
import {GoogleMap} from './GoogleMap';

Enzyme.configure({ adapter: new Adapter()});

describe('<Graph/>', () => {
    it('should not throw error on build', () => {
        expect(() => Enzyme.shallow((
            <GoogleMap zoom={12} center={{lat: 0, lng: 0}} onMarkerClick={undefined} markers={[]}/>
        ))).not.toThrow();
    });
});
