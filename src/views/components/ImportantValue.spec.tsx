import * as Enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import {ImportantValue} from './ImportantValue';
import * as React from 'react';
import {ChartistPie} from './ChartistPie';

describe('<ImportantValue/>', () => {
    beforeAll(() => Enzyme.configure({ adapter: new Adapter()}));

    it('should not throw error on build', () => {
        expect(() => Enzyme.shallow(<ImportantValue id='testValue' name='test value' value={24}/>)).not.toThrow();
    });

    it('should be red if the value is greater than 25.', () => {
        const importantValue = Enzyme.mount(<ImportantValue id='testValue' name='test value' value={26}/>);

        expect(importantValue.find(ChartistPie).props().values[0].className).toBe(ImportantValue.RED_COLOR);
    });

    it('should be yellow if the value is greater than 10 and lesser than 26.', () => {
        const importantValue = Enzyme.mount(<ImportantValue id='testValue' name='test value' value={11} />);

        expect(importantValue.find(ChartistPie).props().values[0].className).toBe(ImportantValue.YELLOW_COLOR);
    });
    it('should be green if the value is lesser than 10.', () => {
        const importantValue = Enzyme.mount(<ImportantValue id='testValue' name='test value' value={7} />);

        expect(importantValue.find(ChartistPie).props().values[0].className).toBe(ImportantValue.GREEN_COLOR);
    });
});
