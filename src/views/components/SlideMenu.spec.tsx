import * as Enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';
import {SlideMenu} from './SlideMenu';

describe('<SlideMenu/>', () => {
    beforeAll(() => Enzyme.configure({ adapter: new Adapter()}));

    it('should not throw error on build', () => {
        expect(() => Enzyme.shallow(<SlideMenu stationName='' listName={[]} menuData={[]} tabContainer={() => (<h1></h1>)}/>)).not.toThrow();
    });
});
