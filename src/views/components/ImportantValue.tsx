import * as React from 'react';
import {isNumber} from 'welshguard';
import {ChartistPie} from './ChartistPie';
import {Utils} from '../../utils/Utils';

export interface ImportantValueProps {
    id: string;
    name: string;
    value: number;
    className?: string;
}

export class ImportantValue extends React.Component<ImportantValueProps> {
    public static GREEN_COLOR = 'chartist-pie-green';
    public static YELLOW_COLOR = 'chartist-pie-yellow';
    public static RED_COLOR = 'chartist-pie-red';
    public static GREY_COLOR = 'chartist-pie-grey';
    public static MAX_VALUE_GRAPH = 200;
    public render(): React.ReactNode {
        const value = isNumber(this.props.value) ? this.props.value : 0;
        const textValue: string = isNumber(this.props.value) ? this.props.value.toString() : 'N\/A';
        return (
            <div className={`${this.props.className} flex-align-center`}>
                <ChartistPie
                    id={this.props.id}
                    totalGraph={ImportantValue.MAX_VALUE_GRAPH}
                    values={[
                        {
                            value,
                            className: Utils.getColor(value, {bad: ImportantValue.RED_COLOR, warning: ImportantValue.YELLOW_COLOR, good: ImportantValue.GREEN_COLOR}),
                        },
                        {
                            value: this.getGreyValue(value) > 0 ? this.getGreyValue(value) : 0,
                            className: ImportantValue.GREY_COLOR,
                        },
                    ]}
                />
                <p className='important-value-value'>{textValue}</p>
                <p className='important-value-text'>{this.props.name}</p>
            </div>
        );
    }

    private getGreyValue(value: number): number {
        return ImportantValue.MAX_VALUE_GRAPH - value;
    }
}
