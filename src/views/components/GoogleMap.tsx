import Map, {MapOptions} from 'google-map-react';
import * as React from 'react';
import {Utils} from '../../utils/Utils';
import _ = require('underscore');

const mapOptions: MapOptions = {
    zoomControl: true,
    scrollwheel: true,
    draggable: true,
    fullscreenControl: false,
    scaleControl: false,
};

export interface Point {
    lat: number;
    lng: number;
}

export interface Marker {
    id: number;
    value: string;
    name: string;
    location: Point;
}

export interface CustomMapProps {
    zoom: number;
    center: Point;
    onMarkerClick: (marker: Marker) => void;
    markers: Marker[];
}

class CustomMap extends React.Component<CustomMapProps> {
    private map: google.maps.Map;
    private markers: google.maps.Marker[] = [];
    public static RED = 'Red';
    public static YELLOW = 'Yellow';
    public static GREEN = 'Green';
    public static BLUE = 'Blue';

    public componentWillUpdate(oldProps: CustomMapProps): void {
        if (this.map) {
            if (!_.isEqual(oldProps.center, this.props.center)) {
                this.map.setCenter(this.props.center);
            }
            if (!_.isEqual(oldProps.zoom, this.props.zoom)) {
                this.map.setZoom(this.props.zoom);
            }
        }
    }

    public render(): React.ReactNode {
        if (this.map) {
            this.renderBody(this.props.markers);
        }
        return (
            <Map
                bootstrapURLKeys={{key: 'AIzaSyC5V7DQT5rgTzt8tinwoBD6pTVbkRZOtIg'}}
                yesIWantToUseGoogleMapApiInternals
                zoom={this.props.zoom}
                defaultZoom={this.props.zoom}
                center={this.props.center}
                defaultCenter={this.props.center}
                options={mapOptions}
                onGoogleApiLoaded={({map}) => {
                    this.map = map;
                }}
            />
        );
    }
    private renderBody(markers: Marker[]): void {
        this.markers.map((marker) => marker.setMap(null));
        this.markers = markers.map(child => {
            const marker = new google.maps.Marker({
                position: child.location,
                clickable: true,
                title: child.name,
                label: {
                    text: child.value,
                    fontSize: '10px',
                },
                icon: `/images/marker${Utils.getColor(child.value !== 'N\/A' ? parseInt(child.value) : undefined, {bad: GoogleMap.RED, warning: GoogleMap.YELLOW, good: GoogleMap.GREEN, notDefined: GoogleMap.BLUE})}.png`,
            });
            marker.addListener('click', () => this.props.onMarkerClick(child));
            marker.setMap(this.map);
            return marker;
        });
    }
}

export const GoogleMap = CustomMap;
