import * as Enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import {SideMenuConnected} from './SideMenu';
import * as React from 'react';

describe('<SideMenu/>', () => {
    beforeAll(() => Enzyme.configure({ adapter: new Adapter()}));
    it('should not throw error on build', () => {
        expect(() => Enzyme.shallow(<SideMenuConnected/>)).not.toThrow();
    });
});
