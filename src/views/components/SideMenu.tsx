import * as React from 'react';
import Drawer from '@material-ui/core/Drawer/Drawer';
import {RevolvairState} from '../AppReducer';
import {DashboardAction, changeDrawer} from '../applications/DashboardActions';
import {Dispatch} from 'redux';
import {withRouter, RouteComponentProps} from 'react-router-dom';
import {StaticContext} from 'react-router';
import {connect} from 'react-redux';
import IconButton from '@material-ui/core/IconButton/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import HomeIcon from '@material-ui/icons/Home';
import SecurityIcon from '@material-ui/icons/Security';
import InfoIcon from '@material-ui/icons/InfoOutlined';
import MailIcon from '@material-ui/icons/Mail';
import List from '@material-ui/core/List/List';
import ListItem from '@material-ui/core/ListItem/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText/ListItemText';
import {Dashboard} from '../applications/Dashboard';

interface DrawerStateToProps {
    drawer: boolean;
}

const mapStateToProps = ({dashboard}: RevolvairState): DrawerStateToProps => ({
    drawer: dashboard.drawer,
});

interface DrawerDispatchToProps {
    changeDrawer: (drawer: boolean) => DashboardAction;
}

const mapDispatchToProps = (dispatch: Dispatch): DrawerDispatchToProps => ({
    changeDrawer: (drawer: boolean) => dispatch(changeDrawer(drawer)),
});

export interface SideMenuContainerProps extends RouteComponentProps<any, StaticContext, any>, DrawerDispatchToProps, DrawerStateToProps {
}

const ICON_WHITE_COLOR = 'secondary';

export class SideMenu extends React.Component<SideMenuContainerProps> {
    public render(): React.ReactNode {
        return (
            <div>
                <IconButton
                    aria-label='Menu'
                    onClick={() => {
                        this.props.changeDrawer(true);
                }}>
                    <MenuIcon color='secondary'/>
                </IconButton>
                <Drawer
                    open={this.props.drawer}
                    onClose={() => {
                        this.props.changeDrawer(false);
                }}>
                    <div
                        tabIndex={0}
                        role='button'
                        onClick={() => {
                            this.props.changeDrawer(false);
                    }}>
                        <List>
                            <ListItem>
                                <img src='/images/logo.svg' className='image'/>
                                <ListItemText primary='Menu'/>
                            </ListItem>
                            <ListItem
                                button
                                onClick={() => this.props.history.location.pathname === Dashboard.HOME_PAGE ? '' : this.props.history.push(Dashboard.HOME_PAGE)
                            }>
                                <ListItemIcon>
                                    <HomeIcon color={ICON_WHITE_COLOR}/>
                                </ListItemIcon>
                                <ListItemText primary='Accueil'/>
                            </ListItem>
                            <ListItem
                                button
                                onClick={() => this.props.history.location.pathname === Dashboard.INFO_PAGE ? '' : this.props.history.push(Dashboard.INFO_PAGE)
                            }>
                                <ListItemIcon>
                                    <InfoIcon color={ICON_WHITE_COLOR}/>
                                </ListItemIcon>
                                <ListItemText primary='À propos'/>
                            </ListItem>
                            <ListItem
                                button
                                onClick={() => this.props.history.location.pathname === Dashboard.ADMIN_PAGE ? '' : this.props.history.push(Dashboard.ADMIN_PAGE)
                            }>
                                <ListItemIcon>
                                    <SecurityIcon color={ICON_WHITE_COLOR}/>
                                </ListItemIcon>
                                <ListItemText primary='Token'/>
                            </ListItem>
                            <ListItem
                                button
                                onClick={() =>
                                    this.props.history.location.pathname === Dashboard.SUBSCRIPTION_PAGE ? '' : this.props.history.push(Dashboard.SUBSCRIPTION_PAGE)
                            }>
                                <ListItemIcon>
                                    <MailIcon color={ICON_WHITE_COLOR}/>
                                </ListItemIcon>
                                <ListItemText primary='Info-lettre'/>
                            </ListItem>
                        </List>
                    </div>
                </Drawer>
            </div>
        );
    }
}

export const SideMenuWithRouter = withRouter(SideMenu);
export const SideMenuConnected = connect(mapStateToProps, mapDispatchToProps)(SideMenuWithRouter);
