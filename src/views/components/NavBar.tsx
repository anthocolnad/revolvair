import Toolbar from '@material-ui/core/Toolbar/Toolbar';
import * as React from 'react';
import {withRouter, RouteComponentProps} from 'react-router-dom';
import {StaticContext} from 'react-router';
import {SideMenuConnected} from './SideMenu';
import {Dashboard} from '../applications/Dashboard';

export interface NavBarProps extends RouteComponentProps<any, StaticContext, any> {
}

export class NavBar extends React.Component<NavBarProps> {
    public render(): React.ReactNode {
        return (
            <nav className='nav-bar'>
                <Toolbar>
                    <SideMenuConnected/>
                    <div
                        className='nav-bar-title'
                        onClick={() => this.props.history.location.pathname === Dashboard.HOME_PAGE ? '' : this.props.history.push(Dashboard.HOME_PAGE)
                    }>
                        <img src='/images/logo.svg' className='image' />
                    </div>
                    <div
                        className='nav-bar-title ml2'
                        onClick={() => this.props.history.location.pathname === Dashboard.HOME_PAGE ? '' : this.props.history.push(Dashboard.HOME_PAGE)
                    }>
                        <div className='flex-align-center'>
                            <h1>RevolvAir</h1>
                            <h2 className='description'>technologies pour un air pur</h2>
                        </div>
                    </div>
                </Toolbar>
            </nav>
        );
    }
}

export const NavBarWithRouter = withRouter(NavBar);
