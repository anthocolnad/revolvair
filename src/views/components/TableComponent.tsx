import * as React from 'react';
import Table from '@material-ui/core/Table/Table';
import Paper from '@material-ui/core/Paper/Paper';
import TableHead from '@material-ui/core/TableHead/TableHead';
import TableRow from '@material-ui/core/TableRow/TableRow';
import TableCell from '@material-ui/core/TableCell/TableCell';
import TableBody from '@material-ui/core/TableBody/TableBody';
import {Tooltip, TableSortLabel} from '@material-ui/core';
import {isDefined} from 'welshguard';

export type orderDirection = 'asc' | 'desc';

export interface HeadObject<T> {
    title: string;
    isSortable?: boolean;
    onClick?: (sortOrder: orderDirection, sortOrderRow: number) => void;
}

export interface TableContainerProps<T> {
    defaultOrderDirection?: orderDirection;
    defaultOrderRow?: number;
    itemTable: T[];
    className?: string;
    onSort?: (items: T[]) => T[];
    rowRender: (props: T, index?: number) => React.ReactNode;
    headTable: HeadObject<T>[];
}

export class TableComponent<T extends object> extends React.Component<TableContainerProps<T>> {
    public static ASCENDANT_SORT: orderDirection = 'asc';
    public static DESCENDANT_SORT: orderDirection = 'desc';
    private sortOrder: orderDirection = this.props.defaultOrderDirection;
    private sortOrderRow: number = this.props.defaultOrderRow;

    public render(): React.ReactNode {
        return (
            <Paper className={['table-component', this.props.className].join(' ')}>
                <Table>
                    <TableHead>
                        <TableRow>
                            {
                                isDefined(this.sortOrder) ?
                                (
                                    this.props.headTable.map((name, index) => {
                                        if (name.isSortable) {
                                            return (
                                                <TableCell key={name.title}>
                                                    <Tooltip title='Trier'>
                                                        <TableSortLabel
                                                            active={this.props.defaultOrderRow === index}
                                                            direction={this.props.defaultOrderDirection}
                                                            onClick={() => {
                                                                this.changeSort(index);
                                                                name.onClick(this.sortOrder, this.sortOrderRow);
                                                            }}
                                                        >
                                                            {name.title}
                                                        </TableSortLabel>
                                                    </Tooltip>
                                                </TableCell>
                                            );
                                        } else {
                                            return (
                                                <TableCell key={name.title}>
                                                    {name.title}
                                                </TableCell>
                                            );
                                        }
                                    })
                                )
                                :
                                (
                                    this.props.headTable.map((name) => {
                                        return (
                                            <TableCell key={name.title}>
                                                {name.title}
                                            </TableCell>
                                        );
                                    })
                                )
                            }
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            isDefined(this.props.onSort) ? (
                                this.props.onSort(this.props.itemTable).map((item, index) => this.props.rowRender(item, index))
                            )
                            :
                            (
                                this.props.itemTable.map((item, index) => this.props.rowRender(item, index))
                            )
                        }
                    </TableBody>
                </Table>
            </Paper>
        );
    }

    private changeSort(index: number): void {
        if (this.sortOrderRow === index) {
            if (this.sortOrder === TableComponent.ASCENDANT_SORT) {
                this.sortOrder = TableComponent.DESCENDANT_SORT;
            } else {
                this.sortOrder = TableComponent.ASCENDANT_SORT;
            }
        } else {
            this.sortOrder = TableComponent.ASCENDANT_SORT;
            this.sortOrderRow = index;
        }
    }
}
