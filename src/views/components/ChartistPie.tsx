import * as React from 'react';
import {Pie} from 'chartist';

export interface ChartistPieProps {
    id: string;
    totalGraph: number;
    values: {
        value: number;
        className?: string;
    }[];
}

export class ChartistPie extends React.Component<ChartistPieProps> {
    public componentDidMount() {
        new Pie(`#${this.props.id}`, {
            series: this.props.values.map((value) => ({
                value: value.value > this.props.totalGraph ? this.props.totalGraph : value.value,
                className: value.className,
            })),
        }, {
            donut: true,
            donutWidth: 5,
            startAngle: 270,
            total: this.props.totalGraph * 2,
            showLabel: false,
        });
    }
    public render(): React.ReactNode {
        return (
            <div className='chartist-pie'>
                <div id={this.props.id}/>
            </div>
        );
    }
}
