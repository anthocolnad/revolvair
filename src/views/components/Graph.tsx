import Paper from '@material-ui/core/Paper/Paper';
import * as React from 'react';
import {CartesianGrid, Line, LineChart, ReferenceLine, ResponsiveContainer, Tooltip as MuiTooltip, XAxis, YAxis} from 'recharts';
import {isDefined, isNumber} from 'welshguard';
import {InformationIcon} from './InformationIcon';
import * as _ from 'underscore';

export interface ImportantValues {
    min: number;
    max: number;
    avg: number;
}


export interface GraphData {
    name: string;
    ['PM2.5']: number;
    tooltip?: ImportantValues;
}

export interface GraphContainerProps {
    dataKeyX: string;
    dataKeyY: string;
    dataDescription: string;
    data: GraphData[];
    title: string;
    colorLine: string;
    width: number;
}

export class Graph extends React.Component<GraphContainerProps> {
    public render(): React.ReactNode {

        return (
            <div className='pb4'>
                <div className='flex pt2'>
                    <h3 className='my2 ml2'>{this.props.title}</h3>
                    <InformationIcon textBubble={this.props.dataDescription} />
                </div>
                {this.renderGraph()}
            </div>
        );
    }

    private renderGraph(): React.ReactNode {
        if (_.some(this.props.data, data => isNumber(data['PM2.5']))) {
            return (
                <ResponsiveContainer width={this.props.width} height={300}>
                    <LineChart data={this.props.data}
                        margin={{top: 10, right: 30, left: 20, bottom: 5}}>
                        <XAxis dataKey={this.props.dataKeyX} axisLine={false} stroke='#708F9F' tickLine={false} tickMargin={10}/>
                        <YAxis dataKey={this.props.dataKeyY} type='number' axisLine={false} stroke='#708F9F' tickLine={false}/>
                        <CartesianGrid vertical={false} stroke='#0A3145'/>
                        <MuiTooltip content={this.customTooltip}/>
                        <ReferenceLine y={26} stroke='red'/>
                        <ReferenceLine y={10} stroke='yellow'/>
                        <Line connectNulls={false} type='monotone' dataKey={this.props.dataKeyY} stroke={this.props.colorLine} activeDot strokeWidth={1.6}/>
                    </LineChart>
                </ResponsiveContainer>
            );
        } else {
            return (
                <div className='my3 mx10'>
                    <p>Aucune donnée disponible</p>
                </div>
            );
        }
    }

    private customTooltip(props: {payload: any, active: boolean}) {
        if (isDefined(props.payload) && props.payload.length > 0) {
            return _.map(props.payload, (payload: any) => {
                const tooltip: ImportantValues = payload.payload.tooltip;
                if (isDefined(tooltip)) {
                    return (
                        <Paper
                            style={{backgroundColor: 'black'}}
                            key={_.reduce(tooltip as any, (old, value, key: string) => key + old, '')}
                            className='p2'
                        >
                            <p className='pb1'>Maximum: {tooltip.max}</p>
                            <p className='pb1'>Moyenne: {tooltip.avg}</p>
                            <p>Minimum: {tooltip.min}</p>
                        </Paper>
                    );
                }
            });
        }
    }
}
