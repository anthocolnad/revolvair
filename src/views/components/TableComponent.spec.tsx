import * as Enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import {TableComponent} from './TableComponent';
import * as React from 'react';
import TableSortLabel from '@material-ui/core/TableSortLabel/TableSortLabel';

describe('<TableComponent/>', () => {
    beforeAll(() => Enzyme.configure({ adapter: new Adapter()}));
    it('should not throw error on build', () => {
        expect(() => Enzyme.shallow(<TableComponent headTable={[]} itemTable={[]} rowRender={() => (<h1></h1>)}/>)).not.toThrow();
    });
    it('rowRender should be equals to number of element of itemTable', () => {
        const spyTableComponent = jest.fn((object, index) => (<h1 key={index}></h1>));
        Enzyme.shallow(<TableComponent headTable={[{title: 'test'}]} itemTable={[{test: 'test'}]} rowRender={spyTableComponent}/>);
        expect(spyTableComponent).toHaveBeenCalledTimes(1);
    });

    it('rowRender should be equals to number of element of itemTable with order and should be ascendant', () => {
        const spyTableComponent = jest.fn((object, index) => (<h1 key={index}>{object}</h1>));
        const table = Enzyme.shallow(<TableComponent
            defaultOrderDirection={TableComponent.ASCENDANT_SORT}
            defaultOrderRow={0}
            onSort={(row) => row.sort((one, two) => (one < two ? -1 : 1))}
            headTable={[{title: 'test', isSortable: true}, {title: 'test2', isSortable: false}]}
            itemTable={['a', 'c', 'b']}
            rowRender={spyTableComponent}/>);
        expect(table.find('h1').map(element => element.text())).toEqual(['a', 'b', 'c']);
        expect(spyTableComponent).toHaveBeenCalledTimes(3);
    });

    it('rowRender should be equals to number of element of itemTable with order and should be descendant', () => {
        const spyTableComponent = jest.fn((object, index) => (<h1 key={index}>{object}</h1>));
        const table = Enzyme.shallow(<TableComponent
            defaultOrderDirection={TableComponent.DESCENDANT_SORT}
            defaultOrderRow={0}
            onSort={(row) => row.sort((one, two) => (one > two ? -1 : 1))}
            headTable={[{title: 'test', isSortable: true}, {title: 'test2', isSortable: false}]}
            itemTable={['a', 'c', 'b']}
            rowRender={spyTableComponent}/>);
        expect(table.find('h1').map(element => element.text())).toEqual(['c', 'b', 'a']);
        expect(spyTableComponent).toHaveBeenCalledTimes(3);
    });

    it('sortOrder should be descendant and sortOrderRow should be 0', () => {
        const spyTableComponent = jest.fn((object, index) => (<tr key={index}><td>{object}</td></tr>));
        let orderTest = TableComponent.ASCENDANT_SORT;
        let orderRowTest = 0;
        const table = Enzyme.mount(<TableComponent
            defaultOrderDirection={orderTest}
            defaultOrderRow={orderRowTest}
            onSort={(row) => row.sort((one, two) => (one > two ? -1 : 1))}
            headTable={[{
                title: 'test',
                isSortable: true,
                onClick: (order, orderRow) => (orderTest =  order, orderRowTest = orderRow),
            },
            {
                title: 'test2',
                isSortable: false,
            }]}
            itemTable={['a', 'c', 'b']}
            rowRender={spyTableComponent}/>);
        table.find(TableSortLabel).get(0).props.onClick();
        expect(orderTest).toEqual(TableComponent.DESCENDANT_SORT);
        expect(orderRowTest).toEqual(0);
    });

    it('sortOrder should be ascendant and sortOrderRow should be 0', () => {
        const spyTableComponent = jest.fn((object, index) => (<tr key={index}><td>{object}</td></tr>));
        let orderTest = TableComponent.DESCENDANT_SORT;
        let orderRowTest = 0;
        const table = Enzyme.mount(<TableComponent
            defaultOrderDirection={orderTest}
            defaultOrderRow={orderRowTest}
            onSort={(row) => row.sort((one, two) => (one > two ? -1 : 1))}
            headTable={[{
                title: 'test',
                isSortable: true,
                onClick: (order, orderRow) => (orderTest =  order, orderRowTest = orderRow),
            },
            {
                title: 'test2',
                isSortable: false,
            }]}
            itemTable={['a', 'c', 'b']}
            rowRender={spyTableComponent}/>);
        table.find(TableSortLabel).get(0).props.onClick();
        expect(orderTest).toEqual(TableComponent.ASCENDANT_SORT);
        expect(orderRowTest).toEqual(0);
    });

    it('sortOrder should be ascendant and sortOrderRow should be 1', () => {
        const spyTableComponent = jest.fn((object, index) => (<tr key={index}><td>{object}</td></tr>));
        let orderTest = TableComponent.DESCENDANT_SORT;
        let orderRowTest = 0;
        const table = Enzyme.mount(<TableComponent
            defaultOrderDirection={orderTest}
            defaultOrderRow={orderRowTest}
            onSort={(row) => row.sort((one, two) => (one > two ? -1 : 1))}
            headTable={[{
                    title: 'test',
                    isSortable: true,
                    onClick: (order, orderRow) => (orderTest =  order, orderRowTest = orderRow),
                },
                {
                    title: 'test2',
                    isSortable: true,
                    onClick: (order, orderRow) => (orderTest =  order, orderRowTest = orderRow),
            }]}
            itemTable={['a', 'c', 'b']}
            rowRender={spyTableComponent}/>);
        table.find(TableSortLabel).get(1).props.onClick();
        expect(orderTest).toEqual(TableComponent.ASCENDANT_SORT);
        expect(orderRowTest).toEqual(1);
    });
});
