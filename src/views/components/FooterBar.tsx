import * as React from 'react';
import {withRouter, RouteComponentProps} from 'react-router-dom';
import {StaticContext} from 'react-router';
import {FacebookLike} from './FacebookLike';

export interface FooterBarProps extends RouteComponentProps<any, StaticContext, any> {
}

export class FooterBar extends React.Component<FooterBarProps> {
    public render(): React.ReactNode {
        return (
            <div className='flex-align-center-direction-horizontal footer-bar py2'>
                <h1 className='footer-bar-text'>Copyright © 2019 RevolvAir</h1>
                <FacebookLike
                    className='ml2'
                    appId='RevolvAir-2206425182723346'
                    url='//www.facebook.com/RevolvAir-2206425182723346'
                />
            </div>
        );
    }
}

export const FooterBarWithRouter = withRouter(FooterBar);
