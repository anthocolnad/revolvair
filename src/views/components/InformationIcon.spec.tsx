import * as Enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import {InformationIcon} from './InformationIcon';
import * as React from 'react';

describe('<InformationIcon/>', () => {
    beforeAll(() => Enzyme.configure({ adapter: new Adapter()}));
    it('should not throw error on build', () => {
        expect(() => Enzyme.shallow(<InformationIcon textBubble='' />)).not.toThrow();
    });
});
