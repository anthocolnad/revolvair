import * as React from 'react';
import {Route, RouteComponentProps, Redirect, Switch, withRouter} from 'react-router-dom';
import {DashboardConnected} from './applications/Dashboard';
import {StaticContext} from 'react-router';

export interface AppProps extends Readonly<RouteComponentProps<any, StaticContext, any>> {}

class RevolvairApp extends React.Component<AppProps> {
    public render(): React.ReactNode {
        return (
            <Switch>
                <Route
                    path='/dashboard'
                    render={props => (<DashboardConnected {...{location: props.location}}/>)}
                />
                <Redirect to='/dashboard'/>
            </Switch>
        );
    }
}

export const App = withRouter(RevolvairApp);
