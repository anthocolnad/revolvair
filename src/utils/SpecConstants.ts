import * as moment from 'moment';

export const timestampTest: number = 1560384000000;
export const createMoment = (timestamp: number) => moment(timestamp).zone(-5);
export const hoursInDay: string[] = ['6h', '7h', '8h', '9h', '10h', '11h', '12h', '13h', '14h', '15h', '16h', '17h', '18h', '19h', '20h', '21h', '22h', '23h', '0h', '1h', '2h', '3h', '4h', '5h'];
export const daysInWeek: string[] = ['7/6', '8/6', '9/6', '10/6', '11/6', '12/6', '13/6'];
export const days: string[] = ['15/5', '16/5', '17/5', '18/5', '19/5', '20/5', '21/5', '22/5', '23/5', '24/5', '25/5', '26/5', '27/5', '28/5', '29/5', '30/5', '31/5', '1/6', '2/6', '3/6', '4/6', '5/6', '6/6', '7/6', '8/6', '9/6', '10/6', '11/6', '12/6', '13/6'];
