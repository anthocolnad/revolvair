import * as moment from 'moment';
import {isDefined} from 'welshguard';
import {GraphData, ImportantValues} from '../views/components/Graph';
import {Utils} from './Utils';

export namespace Formatter {
    export interface Info {
        value: number;
        timestamp: moment.Moment;
    }

    export const DataKey = 'PM2.5';

    export const formatImportantValuesFromGraphData = (values: ImportantValues[]) => {
        const definedValues = values.filter(value => isDefined(value));
        const importantValues: ImportantValues = {
            max: Utils.getMAX(...definedValues.map(data => data.max)),
            min: Utils.getMIN(...definedValues.map(data => data.min)),
            avg: Utils.getAVG(...definedValues.map(data => data.avg)),
        };
        return importantValues;
    };

    export const formatMeasures = (measures: {value: number, timestamp: string}[]) => measures.map(({timestamp, value}) => ({
        value,
        timestamp: Utils.createMoment(+timestamp),
    }));

    export function formatGraphData(section: Utils.TimeGap, measures: Info[], format: string, now: moment.Moment, haveTooltip: boolean = false): GraphData[] {
        const names: string[] = Utils.getDatesIn(section, now, format);

        const fullData: GroupData[] = mergeMeasures(
            names.map(name => ({name, values: undefined})),
            measures.filter(data => data.timestamp.isBetween(moment(now).subtract(1, section), now)),
            format,
        );

        return fullData.map(data => {
            if (data.values && data.values.length > 0) {
                return ({
                    name: data.name,
                    [DataKey]: Utils.getAVG(...data.values),
                    tooltip: haveTooltip ? {
                        max: Utils.getMAX(...data.values),
                        min: Utils.getMIN(...data.values),
                        avg: Utils.getAVG(...data.values),
                    } : undefined,
                });
            } else {
                return ({name: data.name, [DataKey]: undefined});
            }
        });
    }

    interface GroupData {
        values: number[];
        name: string;
    }

    export function mergeMeasures(fullData: GroupData[], measures: Info[], format: string): GroupData[] {
        return measures.reduce(
            (oldData, data) => {
                const name = data.timestamp.format(format);
                const index = oldData.findIndex((data: any) => data.name === name);
                const newData: GroupData[] = [...oldData];
                if (index === -1) {
                    newData.push({name, values: [data.value]});
                } else {
                    if (isDefined(newData[index].values)) {
                        newData[index].values.push(data.value);
                    } else {
                        newData[index].values = [data.value];
                    }
                }
                return newData;
            },
            fullData,
        );
    }

    export function formatDateNotification(format: string, timestamp: number) {
        return moment(timestamp).format(format);
    }
}
