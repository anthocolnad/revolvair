import {Utils} from './Utils';
import moment = require('moment');
import {timestampTest, hoursInDay, days, daysInWeek, createMoment} from './SpecConstants';
import {Station} from '../models/Station';
import {Measure} from '../models/Measure';
import {Sensor} from '../models/Sensor';

describe('Utils', () => {
    describe('getColor', () => {
        const bad: string = 'red';
        const warning: string = 'yellow';
        const good: string = 'green';
        const notDefined: string = 'blue';
        const options = {bad, warning, good, notDefined};

        it('Should return undefined option if called with undefined', () => {
            const quality = Utils.getColor(null, options);
            expect(quality).toBe(notDefined);
        });

        it('Should return good option if called with 5', () => {
            const quality = Utils.getColor(5, options);
            expect(quality).toBe(good);
        });

        it('Get color from utils with 16 value to return warning option', () => {
            const quality = Utils.getColor(16, options);
            expect(quality).toBe(warning);
        });

        it('Get color from utils with 33 value to return bad option', () => {
            const quality = Utils.getColor(Utils.BAD_VALUE_AIR + 1, options);
            expect(quality).toBe(bad);
        });

        it('Get color from utils with 10 value to return good option', () => {
            const quality = Utils.getColor(Utils.GOOD_VALUE_AIR, options);
            expect(quality).toBe(good);
        });

        it('Get color from utils with 25 value to return warning option', () => {
            const quality = Utils.getColor(Utils.BAD_VALUE_AIR, options);
            expect(quality).toBe(warning);
        });
    });

    describe('getMAX', () => {
        it('Should return undefined if no params are passed', () => {
            expect(Utils.getMAX()).toBeUndefined();
        });

        it('Should return one if one and zero are passed', () => {
            expect(Utils.getMAX(1, 0)).toBe(1);
        });
    });

    describe('getMIN', () => {
        it('Should return undefined if no params are passed', () => {
            expect(Utils.getMIN()).toBeUndefined();
        });

        it('Should return zero if one and zero are passed', () => {
            expect(Utils.getMIN(1, 0)).toBe(0);
        });
    });

    describe('getAVG', () => {
        it('Should return undefined if no params are passed', () => {
            expect(Utils.getAVG()).toBeUndefined();
        });

        it('Should return two if one, two and three are passed', () => {
            expect(Utils.getAVG(1, 2, 3)).toBe(2);
        });
    });

    describe('getDaysIn', () => {
        let now: moment.Moment;
        beforeEach(() => now = createMoment(timestampTest));

        it('Should return all 23 hours before now', () => {
            const lastDay = Utils.getDatesIn(Utils.TimeGap.Day, now, 'H[h]');

            expect(lastDay).toEqual(hoursInDay);
            expect(lastDay.length).toBe(Utils.LastDayLenght);
        });

        it('Should return all 6 day before now', () => {
            const lastDay = Utils.getDatesIn(Utils.TimeGap.Week, now, 'D/M');

            expect(lastDay).toEqual(daysInWeek);
            expect(lastDay.length).toBe(Utils.LastWeekLenght);
        });

        it('Should return all 29 days before now', () => {
            const lastMonth = Utils.getDatesIn(Utils.TimeGap.Month, now, 'D/M');

            expect(lastMonth).toEqual(days);
            expect(lastMonth.length).toEqual(Utils.LastMonthLenght);
        });
    });
    describe('getLastMeasure', () => {
        const lastMeasure = 2;
        const measures1: Measure = {
            value: lastMeasure,
            timestamp: '10',
        };
        const measures2: Measure = {
            value: lastMeasure + 1111,
            timestamp: '11',
        };
        const sensorWithMeasures: Sensor = {
            id: 1,
            stationId: 1,
            latestMeasures: [measures1, measures2],
        };
        const sensorWithoutMeasures: Sensor = {
            id: 2,
            stationId: 2,
            latestMeasures: [],
        };
        const sensorWithUndefinedMeasures: Sensor = {
            id: 2,
            stationId: 2,
            latestMeasures: undefined,
        };
        const stationWithSensor: Station = {
            id: 1,
            name: 'test',
            lat: 10,
            long: 10,
            sensors: [sensorWithMeasures],
        };
        const stationWithoutSensor: Station = {
            id: 2,
            name: 'test',
            lat: 10,
            long: 10,
        };
        const stationWithtSensorWithoutMeasures: Station = {
            id: 2,
            name: 'test',
            lat: 10,
            long: 10,
            sensors: [sensorWithoutMeasures],
        };
        const stationWithUndefinedSensor: Station = {
            id: 2,
            name: 'test',
            lat: 10,
            long: 10,
            sensors: undefined,
        };
        const stationWithtSensorWithUndefinedMeasures: Station = {
            id: 2,
            name: 'test',
            lat: 10,
            long: 10,
            sensors: [sensorWithUndefinedMeasures],
        };
        it('Should return lastMeasure of 6', () => {
            const lastMeasure = Utils.getLastMeasure(stationWithSensor);

            expect(lastMeasure).toEqual(lastMeasure);
        });
        it('should return null when station has no sensor', () => {
            expect(Utils.getLastMeasure(stationWithoutSensor)).toEqual(null);
        });
        it('should return null when station has sensor but no measures', () => {
            expect(Utils.getLastMeasure(stationWithtSensorWithoutMeasures)).toEqual(null);
        });
        it('should return null when station sensor is undefined', () => {
            expect(Utils.getLastMeasure(stationWithUndefinedSensor)).toEqual(null);
        });
        it('should return null when station has sensor with undefined measures', () => {
            expect(Utils.getLastMeasure(stationWithtSensorWithUndefinedMeasures)).toEqual(null);
        });
    });
});

