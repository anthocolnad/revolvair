import {isNotDefined, isDefined} from 'welshguard';
import _ = require('underscore');
import * as moment from 'moment';
import {Station} from '../models/Station';

export namespace Utils {
    export enum TimeGap {
        Month = 'month',
        Day = 'day',
        Week = 'week',
    }

    export const GOOD_VALUE_AIR = 10;
    export const BAD_VALUE_AIR = 25;

    export function getColor<T>(value: number, options: {bad: T, warning: T, good: T, notDefined?: T}): T {
        if (isNotDefined(value)) {
            return options.notDefined;
        }
        if (value <= GOOD_VALUE_AIR) {
            return options.good;
        } else if (value <= BAD_VALUE_AIR) {
            return options.warning;
        } else {
            return options.bad;
        }
    }

    export function getLastMeasure(station: Station): number {
        if (station.sensors && station.sensors.length > 0) {
            if (station.sensors[0].latestMeasures && station.sensors[0].latestMeasures.length > 0) {
                return station.sensors[0].latestMeasures[0].value;
            }
        }
        return null;
    }

    export const getAVG = (...values: number[]) => {
        const definedValues = values.filter(value => isDefined(value));
        return definedValues.length > 0
            ? Math.floor(_.reduce(definedValues, (memo, num) => memo + num, 0) / definedValues.length)
            : undefined;
    };

    export const getMAX = (...values: number[]) => {
        const definedValues = values.filter(value => isDefined(value));
        return definedValues.length > 0
            ? _.max(definedValues)
            : undefined;
    };

    export const getMIN = (...values: number[]) => {
        const definedValues = values.filter(value => isDefined(value));
        return definedValues.length > 0
            ? _.min(definedValues)
            : undefined;
    };

    export const LastDayLenght: number = 24;
    export const LastWeekLenght: number = 7;
    export const LastMonthLenght: number = 30;

    export function getDatesIn(section: TimeGap, now: moment.Moment, format: string): string[] {
        let dates: moment.Moment[];

        switch (section) {
            case TimeGap.Month:
                dates = [...new Array(LastMonthLenght).keys()].map(name => moment(now).subtract(name, 'day'));
                break;
            case TimeGap.Week:
                dates = [...new Array(LastWeekLenght).keys()].map(name => moment(now).subtract(name, 'day'));
                break;
            case TimeGap.Day:
                dates = [...new Array(LastDayLenght).keys()].map(name => moment(now).subtract(name, 'hour'));
                break;
        }

        return dates.map(date => date.format(format)).reverse();
    }

    export const createMoment = (timestamp: number) => moment(timestamp);
}
