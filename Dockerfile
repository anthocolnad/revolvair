# base image
FROM node:10.1-alpine AS base

RUN apk update
RUN apk upgrade
RUN apk add bash

# Creating work dir
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY ./package.json ./package.json
COPY ./package-lock.json ./package-lock.json

RUN npm install

COPY ./tsconfig.json ./tsconfig.json
COPY ./tslint.json ./tslint.json
COPY ./postcss.config.js ./postcss.config.js
COPY ./webpack.config.js ./webpack.config.js
COPY ./src ./src
COPY ./dist/public/images ./dist/public/images
COPY ./.env ./.env

RUN npm run bp
