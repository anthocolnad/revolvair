#!/usr/bin/ruby -w

require 'net/http'
require 'uri'
require 'json'
require 'date'
require 'optparse'


class Station
    attr_accessor :name, :long, :lat, :id
    def initialize(stationName, id, long, lat)
        @name = stationName
        @id = id
        @long = long || -71.34 
        @lat = lat  || 41.12
    end
end

def do_http_request(uri, request)
    response = Net::HTTP.start(uri.hostname, uri.port) do |http|
        http.request(request)
    end
    return response
end

def get_current_timestamp_millis()
    return (Time.now.getutc.to_f * 1000).round
end

options = {}
OptionParser.new do |opts|
    opts.banner = "Usage: example.rb [options]"

    opts.on("-s stations,measures,month,notifications", "--seed", Array, "Seed database") do |list|
        options[:seed] = list
    end

    opts.on("-c", "--create-index", "Create index") do |c|
        options[:create] = c
    end

    opts.on("-u HOST", "--url", String, "Url") do |u|
        options[:url] = u
    end
end.parse!

##############################
#   Create stations' index   #
##############################
if options.key?(:create)
    uri = URI.join(options[:url], '/revolvairstations')
    request = Net::HTTP::Put.new(uri)
    request.content_type = "application/json"
    request.body = JSON.dump({
        "mappings" => {
            "_default_" => {
                "properties" => {
                    "location" => {
                        "type" => "geo_point"
                    },
                    "stationId" => {
                        "type" => "integer"
                    },
                    "stationName" => {
                        "type" => "text",
                        "fielddata" => true,
                    },
                    "userId" => {
                        "type" => "integer"
                    },
                }
            }
        }
    })
    p do_http_request(uri, request).body

    ###################################
    #   Create notifications' index   #
    ###################################
    uri = URI.join(options[:url], '/revolvairnotifications')
    request = Net::HTTP::Put.new(uri)
    request.content_type = "application/json"
    request.body = JSON.dump({
        "mappings" => {
            "_default_" => {
                "properties" => {
                    "stationId" => {
                        "type" => "integer",
                    },
                    "@timestamp" => {
                        "type" => "date",
                        "format" => "epoch_millis",
                    },
                    "newValue" => {
                        "type" => "float",
                    },
                    "oldValue" => {
                        "type" => "float",
                    },
                    "oldState" => {
                        "type" => "text",
                    },
                    "newState" => {
                        "type" => "text",
                    },
                }
            }
        }
    })
    p do_http_request(uri, request).body

    ##############################
    #   Create measures' index   #
    ##############################
    uri = URI.join(options[:url], '/revolvairmeasures')
    request = Net::HTTP::Put.new(uri)
    request.content_type = "application/json"
    request.body = JSON.dump({
        "mappings" => {
            "_default_" => {
                "properties" => {
                    "@timestamp" => {
                        "type" => "date",
                        "format" => "epoch_millis"
                    },
                    "stationId" => {
                        "type" => "integer",
                    },
                    "sensorId" => {
                        "type" => "integer",
                    },
                    "value" => {
                        "type" => "float"
                    }
                }
            }
        }
    })
    p do_http_request(uri, request).body
end

##########################
#       Seed data        #
##########################

if options.key?(:seed)
    stations = [
        Station.new('Ste-Foy', 1, -71.2792106, 46.7872133), 
        Station.new('Lévis', 2, -71.2616188, 46.7353835), 
        Station.new('Place Ste-Foy', 3, -71.2789328, 46.7768418), 
        Station.new('Shenzhen', 4, 114.0610777, 22.542842),
        Station.new('Mumbai', 5, 72.875781, 19.073482),
        Station.new('Seoul', 6, 126.982627, 37.558484),
        Station.new('Cégep de Ste-foy', 7, -71.286618, 46.786407)
    ]

    if options[:seed].include?('stations')
        uri = URI.join(options[:url], '/revolvairstations/stations/?pretty')
        request = Net::HTTP::Post.new(uri)
        request.content_type = "application/json"
        for station in stations
            request.body = JSON.dump({
                "location" => {
                    "lat" => station.lat,
                    "lon" => station.long
                },
                "stationId" => station.id,
                "stationName" => station.name,
                "userId" => 5,
            })
            p do_http_request(uri, request).body
        end
    end

    if options[:seed].include?('notifications')
        uri = URI.join(options[:url],'/revolvairnotifications/notifications/?pretty') 
        request = Net::HTTP::Post.new(uri)
        request.content_type = "application/json"
        for station in stations
            if station.name != 'Cégep de Ste-foy'
                for i in (2..8)
                    if i * 5 > 25
                        state = 'dangereux'
                    elsif i * 5 >= 10
                        state = 'alerte'
                    else
                        state = 'bon'
                    end

                    if i * 5 > 10
                        request.body = JSON.dump({
                            "stationId" => station.id,
                            "@timestamp" => get_current_timestamp_millis() - ((3600 * 24) * i * 3), 
                            "newValue" => i * 5,
                            "oldValue" => i,
                            "oldState" => 'bon',
                            "newState" => state,
                        })
                        p request.body
                        do_http_request(uri, request)
                    end
                end
            end
        end
    end

    if options[:seed].include?('measures')
        uri = URI.join(options[:url], '/revolvairmeasures/measures/?pretty')
        request = Net::HTTP::Post.new(uri)
        request.content_type = "application/json"
        nbOfMeasure = 200

        for station in stations[0..(stations.length - 3)]
            p station
            for i in 0..nbOfMeasure
                if station.name == 'Bombay' and i > (nbOfMeasure - 15)
                    value = 0
                elsif station.name == 'Shenzhen' and i > (nbOfMeasure - 15)
                    value = 15
                else
                    value = i + 5
                end
                request.body = JSON.dump({
                    "@timestamp" => get_current_timestamp_millis(),
                    "stationId" => station.id,
                    "sensorId" => station.id + 1,
                    "value" => value
                })
                sleep(0.4)
                do_http_request(uri, request)
            end

            if options[:seed].include?('month')
                for i in 0..40
                    time = Time.now.getutc.to_f - ((3600 * 24) * i)
                    request.body = JSON.dump({
                        "@timestamp" => (time * 1000).round,
                        "stationId" => station.id,
                        "sensorId" => station.id + 1,
                        "value" => rand(1..40)
                    })
                    do_http_request(uri, request)
                end
            end
        end
    end
end
