const externals = require("webpack-node-externals");
const WebpackMonitor = require('webpack-monitor');
const _ = require('underscore')

const prod = 'production';
const dev = 'development';

module.exports = () => {
    const args = process.argv;
    const analyse = _.contains(args, '-a');
    const inProd = _.contains(args, '-p');

    const mode = process.env.NODE_ENV === prod || inProd ? prod : dev;
    const plugins = [];

    if (analyse) {
        plugins.push(new WebpackMonitor({launch: true}));
    }

    const common = {
        mode,
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    loader: "awesome-typescript-loader"
                },
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: 'eslint-loader',
                    enforce: 'pre'
                },
                {
                    test: /\.scss$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: 'public/css/styles.bundle.css',
                            }
                        },
                        {
                            loader: 'extract-loader'
                        },
                        {
                            loader: 'css-loader?-url'
                        },
                        {
                            loader: 'postcss-loader'
                        },
                        {
                            loader: 'sass-loader'
                        }
                    ]
                },
                {
                    enforce: 'pre',
                    test: /\.ts(x?)$/i,
                    exclude: [/node_modules/],
                    use: {
                        loader: 'tslint-loader',
                        options: {
                            configFile: './tslint.json',
                            tsConfigFile: './tsconfig.json',
                            emitErrors: true,
                            failOnHint: false,
                            fix: true,
                        },
                    },
                },
            ]
        },
        devtool: "source-map",
        resolve: {
            extensions: ['.scss', '.ts', '.tsx', '.js'],
        }
    };
    
    const web = {
        plugins,
        entry: [`./src/FrontendEntry.tsx`, `./src/StylesEntry.scss`],
        output: {
		    filename: 'public/js/frontend.bundle.js',
        },
        target: 'web',
        externals: [],
        // optimization: {
        //     splitChunks: {
        //         name: true,
        //         cacheGroups: {
        //             commons: {
        //                 chunks: 'initial',
        //                 minChunks: 2
        //             },
        //             vendors: {
        //                 test: /[\\/]node_modules[\\/]/,
        //                 chunks: 'all',
        //                 priority: -10
        //             }
        //         }
        //     },
        //     runtimeChunk: true
        // },
    }
    
    const node = {
        entry: [`./src/ServerEntry.tsx`],
        output: {
            filename: './server.bundle.js',
        },
        target: 'node',
        externals: [externals()],
    };

    return [
        Object.assign({}, common, web),
        Object.assign({}, common, node),
    ];
}
